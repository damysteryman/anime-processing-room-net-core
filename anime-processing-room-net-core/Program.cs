﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace anime_processing_room_net_core
{
    class MainClass
    {        
        public static void Main(string[] args)
        {
            string src = "";
            string dest = "";
            string name = "";
            int season = 0;
            int? epOff = null;
            int? chpSrcIdx = null;
            int epMin = 0;
            int epMax = 0;
            string epTitleFormat = "";
            string srcFilePrefixFormat = "";
            string destFilenameFormat = "";
            bool overwrite = false;
            bool verbose = false;
            bool sim = false;
            string[] mkvmergePaths = new string[3];
            string sliceTxtPath = "";
            string titlesTxtPath = "";
            string trackOrdersPath = "";
            string trackSourceTablePath = "";
            string ceDefaultsTablePath = "";
            string syncTimesPath = "";
            string dynTrackTitlesPath = "";
            string metaDir = "";
            string srcBase = "";
            string chpDir = "";
            List<int> skipEps = new List<int>();

            ConsolePrint(UI.Banner());

            if (args.Length < 1)
            {
                ConsolePrint(UI.Usage());
                    return;
            }
            else
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "--help":
                            ConsolePrint(UI.Usage());
                            return;

                        case "--name":
                        case "-n":
                            name = args[++i];
                            break;

                        case "--season":
                            season = int.Parse(args[++i]);
                            break;

                        case "--dest":
                        case "-d":
                            dest = args[++i];
                            break;

                        case "--ep-min":
                            epMin = int.Parse(args[++i]);
                            break;

                        case "--ep-max":
                            epMax = int.Parse(args[++i]);
                            break;

                        case "--ep-off":
                            epOff = int.Parse(args[++i]);
                            break;

                        case "--chapter-source-index":
                            chpSrcIdx = int.Parse(args[++i]);
                            break;

                        case "--ep-title-format":
                            epTitleFormat = args[++i];
                            break;

                        case "--src-file-pref-format":
                            srcFilePrefixFormat = args[++i];
                            break;

                        case "--dest-filename-format":
                            destFilenameFormat = args[++i];
                            break;

                        case "--verbose":
                            verbose = true;
                            break;

                        case "--simulate":
                            sim = true;
                            verbose = true;
                            break;

                        case "--overwrite":
                            overwrite = true;
                            break;

                        case "--mkvmerge":
                            mkvmergePaths[0] = args[++i];
                            break;

                        case "--mkvinfo":
                            mkvmergePaths[1] = args[++i];
                            break;

                        case "--mkvextract":
                            mkvmergePaths[2] = args[++i];
                            break;

                        case "--sources-file":
                            src = args[++i];
                            break;

                        case "--trim-file":
                            sliceTxtPath = args[++i];
                            break;

                        case "--titles-file":
                            titlesTxtPath = args[++i];
                            break;

                        case "--torders-file":
                            trackOrdersPath = args[++i];
                            break;

                        case "--tst-file":
                            trackSourceTablePath = args[++i];
                            break;

                        case "--cedt-file":
                            ceDefaultsTablePath = args[++i];
                            break;

                        case "--dtt-file":
                            dynTrackTitlesPath = args[++i];
                            break;

                        case "--syncs-file":
                            syncTimesPath = args[++i];
                            break;

                        case "--meta-dir":
                            metaDir = args[++i];
                            break;

                        case "--source-base-dir":
                            srcBase = args[++i];
                            break;

                        case "--chapters-dir":
                            chpDir = args[++i];
                            break;

                        case "--skip":
                            string[] temp = args[++i].Split(',');
                            foreach (string epNo in temp)
                                try
                                {
                                    skipEps.Add(int.Parse(epNo));
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine($"WARNING: Specified Skipped Episode '{epNo}' is not a valid number, not adding to Episode Skip list.");
                                }
                            break;

                        default:
                            Console.WriteLine($"WARNING: Specified arg '{args[i]}' not supported, ignoring it...");
                            break;
                    }
                }

            if (string.IsNullOrEmpty(dest))
            {
                Console.WriteLine("ERROR! Destination directory not specified!");
                Console.WriteLine("Specify it with the --dest argument.");
                Console.WriteLine($"Unable to continue, now exiting...");
                Environment.Exit(-1);
            }

            if (epMin < 1)
            {
                Console.WriteLine("ERROR! Mininum/start episode number not specified!");
                Console.WriteLine("Specify it with the --ep-min argument.");
                Console.WriteLine($"Unable to continue, now exiting...");
                Environment.Exit(-1);
            }

            if (epMax < 1)
            {
                Console.WriteLine("ERROR! Maximum/ending episode number not specified!");
                Console.WriteLine("Specify it with the --ep-max argument.");
                Console.WriteLine($"Unable to continue, now exiting...");
                Environment.Exit(-1);
            }
            
            AnimeProcessingRoom apr = new AnimeProcessingRoom(
                                    new AnimeProcessingRoomArgs(
                                        dest, 
                                        epMin, 
                                        epMax, 
                                        name, 
                                        season,
                                        skipEps,
                                        epOff,
                                        chpSrcIdx,
                                        epTitleFormat,
                                        srcFilePrefixFormat,
                                        destFilenameFormat,
                                        metaDir,
                                        srcBase,
                                        src,
                                        titlesTxtPath,
                                        sliceTxtPath,
                                        trackOrdersPath,
                                        trackSourceTablePath,
                                        ceDefaultsTablePath,
                                        dynTrackTitlesPath,
                                        syncTimesPath,
                                        chpDir,
                                        mkvmergePaths
                                        ));
            apr.OnMessage += apr_OnMessage;

            if (apr.LoadData())
            {
                for (int i = 0; i < apr.Settings.EpisodeCount; i++)
                {   
                    // Tell APR to load all metadata for this episode and process it
                    if (apr.ProcessEpisode(i))
                    {
                        // Display Processed Episode metadata
                        ConsolePrint(UI.FileBanner(apr, i));
                        Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0);

                        // Display Sources used for this Episode
                        ConsolePrint(UI.SourceTable(apr, i).PutInBorder());
                        Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0);

                        // Display all Tracks for all Sources used for this Episode
                        TrackTable trackTable = new TrackTable(apr, i);
                        ConsolePrint(trackTable.ToStringList("SOURCE TRACKS").PutInBorder());
                        Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0);

                        // Display the Tracks that will be in the output MKV file, and in what order
                        ConsolePrint(trackTable.GetFinalSortedTable(apr.TrackOrders[i]).ToStringList("FINAL OUTPUT TRACKS").PutInBorder());
                        Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0);

                        // Display the Chapter data for for this Episode
                        ConsolePrint(UI.ChapterTable(apr, i).PutInBorder());
                        Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0);

                        // Display the args that will be passed to mkvmerge if verbose
                        if (verbose)
                        {
                            ConsolePrint("VERBOSE: Args being passed to mkvmerge:".PutInBorder(Console.WindowWidth));
                            foreach (string arg in apr.EpisodeProcessArgs[i])
                                Console.WriteLine(arg);
                            Console.WriteLine(string.Concat(Enumerable.Repeat("=", Console.WindowWidth)));
                        }

                        // Build the output MKV file if not simulation mode
                        if (!sim)
                        {
                            ConsolePrint("BUILD WITH mkvmerge:".PutInBorder(Console.WindowWidth));
                            apr.BuildEpisode(i, overwrite);
                            Console.WriteLine(string.Concat(Enumerable.Repeat("=", Console.WindowWidth)));
                        }
                    }
                }
            }
        }

        private static void apr_OnMessage(object sender, MsgArgs e)
        {
            Console.WriteLine(e.Message);

            Regex r = new Regex(@"Progress: [0-9]{1,3}%");
            if (!string.IsNullOrEmpty(e.Message) && r.Match(e.Message).Success)
                Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0); 
        }

        private static void ConsolePrint(List<string> stringList)
        {
            foreach (string s in stringList)
                Console.WriteLine(s);
        }
    }
}
