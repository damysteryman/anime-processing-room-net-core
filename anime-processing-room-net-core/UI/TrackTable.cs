using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace anime_processing_room_net_core
{
    public class TrackTable
    {
        public List<List<Track>> Tracks { get; set; }
        public int Width;
        public int Height;

        public TrackTable()
        {
            Tracks = new List<List<Track>>();
        }

        public TrackTable(AnimeProcessingRoom apr, int epIdx)
        {
            Tracks = new List<List<Track>>();
            AddAllSourceTracks(apr, epIdx);
        }

        public void AddAllSourceTracks(AnimeProcessingRoom apr, int epIdx)
        {
            List<Source> sources = new List<Source>();
            List<int> tst = new List<int>();
            for (int i = 0; i < apr.Sources.Count; i++)
                if (apr.EpisodeEnabledSources[epIdx][i])
                {
                    sources.Add(apr.Sources[i]);
                    tst.Add(apr.TrackSourceTable[epIdx][i]);
                }

            for (int i = 0; i < sources.Count; i++)
            {
                List<Track> newTrackList = new List<Track>();
                foreach (Track t in sources[i].Tracks[tst[i]])
                {
                    if (t == null)
                        newTrackList.Add(null);
                    else
                    {
                        string[] dtt = apr.DynTrackTitles[epIdx];
                        string name = t.Name;
                        string source = t.Source;
                            if (dtt != null)
                                for (int k = 0; k < dtt.Length; k++)
                                {
                                    name = name.Replace($"{{{k}}}", dtt[k]);
                                    source = source.Replace($"{{{k}}}", dtt[k]);
                                }
                                    

                        newTrackList.Add(new Track
                                            (
                                                $"{i}:{t.ID.Split(':')[1]}",
                                                t.Type,
                                                t.Language,
                                                name,
                                                t.IsDefault,
                                                t.IsForced,
                                                t.IsHearingImpaired,
                                                t.IsVisualImpaired,
                                                t.IsTextDescriptions,
                                                t.IsOriginalLang,
                                                t.IsCommentary,
                                                source
                                            )
                                        );
                    }
                }

                Tracks.Add(newTrackList);
            }
        }

        public TrackTable GetFinalSortedTable(string trackOrder)
        {
            Regex format = new Regex(@"[0-9]+:[0-9]+");

            List<Track> temp = new List<Track>();
            List<Track> final = new List<Track>();
            foreach (List<Track> tl in Tracks)
                foreach (Track tr in tl)
                    if (tr != null)
                        temp.Add(tr);

            TrackTable finalTable = new TrackTable();
            finalTable.Tracks.Add(new List<Track>());

            string[] orders = trackOrder.Split(',');
            for (int i = 0; i < orders.Length; i++)
            {
                if (format.Match(orders[i]).Success)
                {
                    int trkIdx = temp.FindIndex(0, temp.Count, t => t.ID.Equals(orders[i]));
                    final.Add(temp[trkIdx]);
                    temp.RemoveAt(trkIdx);
                }
                else
                    throw new Exception($"{(i+1).Ordinal()} TrackOrder '{orders[i]}' is invalid. Valid format is 'SourceIndexValue:TrackIndexValue'.");
            }
            final.AddRange(temp);
            finalTable.Tracks[0] = final;
            return finalTable;
        }

        public List<string> ToStringList(string _title)
        {
            List<string> lines = new List<string>();
            string[] headers = new string[] { "ID", "TYPE", "LANG", "NAME", "FLAGS", "SOURCE" };
            int[] cols_len = new int[] { 4, 8, 4, 4, 13, 6 };
            
            for (int i = 0; i < Tracks.Count; i++)
            {
                for (int j = 0; j < Tracks[i].Count; j++)
                {
                    if (Tracks[i][j] != null)
                    {
                        Track t = Tracks[i][j];

                        int[] cells_len = new int[] {
                            i.ToString().Length + j.ToString().Length + 1,
                            t.Type.ToString().Length,
                            t.Language.Length,
                            t.Name.Length,
                            cols_len[4],
                            t.Source.Length
                        };

                        for (int k = 0; k < cells_len.Length; k++)
                            cols_len[k] = cells_len[k] > cols_len[k] ? cells_len[k] : cols_len[k];
                    }
                }
            }
            string col_sep = " | ";
            Width = cols_len.Sum() + (col_sep.Length * (cols_len.Length - 1));
            string row_sep = string.Concat(Enumerable.Repeat("-", Width));
            
            // Add Track title to table
            lines.Add(_title);
            lines.Add(row_sep);
            
            // Create table header
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < headers.Length; i++)
            {
                sb.Append(headers[i] + string.Concat(Enumerable.Repeat(" ", cols_len[i] - headers[i].Length)));
                if (i < headers.Length - 1)
                    sb.Append(col_sep);
            }
            lines.Add(sb.ToString());
            lines.Add(row_sep);

            // Add rows to table
            for (int i = 0; i < Tracks.Count; i++)
            {
                for (int j = 0; j < Tracks[i].Count; j++)
                {
                    sb = new StringBuilder();
                    
                    string[] cells;
                    Track t = Tracks[i][j];
                    if (t != null)
                    {
                        string flags =  (t.IsDefault ? "D" : " ") + 
                                        (t.IsForced ? " F" : "  ") + 
                                        (t.IsHearingImpaired ? " H" : "  ") + 
                                        (t.IsVisualImpaired ? " V" : "  ") + 
                                        (t.IsTextDescriptions ? " T" : "  ") + 
                                        (t.IsOriginalLang ? " O" : "  ") + 
                                        (t.IsCommentary ? " C" : "  ");
                        cells = new string[]
                        {
                            t.ID,
                            t.Type.ToString(),
                            t.Language,
                            t.Name,
                            flags,
                            t.Source
                        };
                    }
                    else
                        cells = new string[]
                        {
                            $"{i}:{j}",
                            "IGNORED",
                            "",
                            "",
                            "",
                            ""
                        };

                    for (int k = 0; k < headers.Length; k++)
                    {
                        sb.Append(cells[k] + string.Concat(Enumerable.Repeat(" ", cols_len[k] - cells[k].Length)));
                        if (k < headers.Length - 1)
                            sb.Append(col_sep);
                    }

                    lines.Add(sb.ToString());
                }
            }
            return lines;
        }
    }
}