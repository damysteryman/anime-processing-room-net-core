using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace anime_processing_room_net_core
{
    public static class UI
    {
        public static List<string> Banner()
        {
            string str = $"Ver: {((AssemblyInformationalVersionAttribute)(Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyInformationalVersionAttribute)))).InformationalVersion}";
            string author = $"By {((AssemblyCompanyAttribute)(Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyCompanyAttribute)))).Company}";
            List<string> banner = new List<string> { "ANIME PROCESSING ROOM", str, author };
            banner.PutInBorder();
            return banner;
        }

        public static List<string> PutInBorder(this string _str, int _width = -1, bool _startBorder = true, bool _endBorder = true)
        {
            if (_width < 0)
                _width = _str.Length + 4;
                
            List<string> strs = new List<string> { _str };
            strs.PutInBorder(_width, _startBorder, _endBorder);

            return strs;
        }

        public static List<string> PutInBorder(this List<string> _strings, int _width = -1, bool _startBorder = true, bool _endborder = true)
        {
            if (_strings.Count > 0)
            {
                if (_width < 0)
                {
                    _width = _strings.Max(val => val.Length) + 4;
                }

                string border = "=";
                while (border.Length < _width)
                    border += "=";

                for (int i = 0; i < _strings.Count; i++)
                {
                    int len = _width - 1;
                    if (!_strings[i].StartsWith("--"))
                        _strings[i] = " " + _strings[i];
                    _strings[i] = "=" + _strings[i];
                    while (_strings[i].Length < len)
                        if (_strings[i].EndsWith("--"))
                            _strings[i] += "-";
                        else
                            _strings[i] += " ";
                    if (_strings[i].Length == len)
                        _strings[i] += "=";
                }
                if (_startBorder) _strings.Insert(0, border);
                if (_endborder) _strings.Add(border);
            }
            
            return _strings;
        }

        public static List<string> FileBanner(AnimeProcessingRoom apr, int epIdx)
        {
            int linelen = (Console.WindowWidth - 2) > 0 ? (Console.WindowWidth - 2) : 0;
            
            List<string> fileBanner = new List<string>();
            fileBanner.Add($"File No. {epIdx + 1} of {apr.Settings.EpisodeCount}");
            fileBanner.Add(string.Concat(Enumerable.Repeat("-", linelen)));
            fileBanner.Add($"EPISODE     | {apr.EpisodeNumbers[epIdx]}");
            fileBanner.Add($"TRACK ORDER | {(apr.TrackOrders != null ? apr.TrackOrders[epIdx] : "(none)")}");
            fileBanner.Add($"TRIM TIMES  | {(!string.IsNullOrEmpty(apr.TrimTimes[epIdx]) ? apr.TrimTimes[epIdx] : "(none)")}");
            fileBanner.Add($"FILENAME    | {apr.EpisodeFileNames[epIdx]}");
            fileBanner.Add($"VIDEO TITLE | {(!string.IsNullOrEmpty(apr.EpisodeFullTitles[epIdx]) ? apr.EpisodeFullTitles[epIdx] : "(none)")}");
            fileBanner.PutInBorder(Console.WindowWidth);

            return fileBanner;
        }

        public static List<string> SourceTable(AnimeProcessingRoom apr, int epIdx)
        {
            List<string> lines = new List<string>();
            string[] headers = new string[] { "IDX", "TRACKLIST IDX", "TRACKLIST FILE", "SOURCE FILE PATH", "SYNC " };
            int[] cols_len = new int[] { headers[0].Length, headers[1].Length, headers[2].Length, headers[3].Length, headers[4].Length };

            List<Source> sources = new List<Source>();
            List<int> tst = new List<int>();
            List<int> syncs = new List<int>();
            for (int i = 0; i < apr.Sources.Count; i++)
                if (apr.EpisodeEnabledSources[epIdx][i])
                {
                    sources.Add(apr.Sources[i]);
                    tst.Add(apr.TrackSourceTable[epIdx][i]);
                    syncs.Add(i > 0 ? apr.Syncs[epIdx][i - 1] : 0);
                }
                    
            // Calculate column and row sizes
            for (int i = 0; i < sources.Count; i++)
            {
                int[] cells_len = new int[] {
                                                i.ToString().Length,
                                                tst[i].ToString().Length,
                                                sources[i].TrackMetaFiles[tst[i]].Length,
                                                sources[i].SrcPath.Length,
                                                i > 0 ? syncs[i].ToString("+#;-#;0").Length : cols_len[4]
                                            };
                for (int j = 0; j < cells_len.Length; j++)
                    cols_len[j] = cells_len[j] > cols_len[j] ? cells_len[j] : cols_len[j];
            }
            
            string col_sep = " | ";
            int Width = cols_len.Sum() + (col_sep.Length * (cols_len.Length - 1));
            string row_sep = string.Concat(Enumerable.Repeat("-", Width));

            // Add Source title to table
            lines.Add("SOURCES");
            lines.Add(row_sep);
            
            // Create table header
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < headers.Length; i++)
            {
                sb.Append(headers[i] + string.Concat(Enumerable.Repeat(" ", cols_len[i] - headers[i].Length)));
                if (i < headers.Length - 1)
                    sb.Append(col_sep);
            }
            lines.Add(sb.ToString());
            lines.Add(row_sep);

            // Add rows to table
            for (int i = 0; i < sources.Count; i++)
            {
                sb = new StringBuilder();
                string[] cells = new string[]   {
                                                    i.ToString(),
                                                    tst[i].ToString(),
                                                    sources[i].TrackMetaFiles[tst[i]],
                                                    sources[i].SrcPath,
                                                    i > 0 ? syncs[i].ToString("+#;-#;0") : "BASE"
                                                };
                for (int j = 0; j < headers.Length; j++)
                {
                    sb.Append(cells[j] + string.Concat(Enumerable.Repeat(" ", cols_len[j] - cells[j].Length)));
                    if (j < headers.Length - 1)
                        sb.Append(col_sep);
                }

                lines.Add(sb.ToString());
            }

            return lines;
        }
        
        public static List<string> ChapterTable(AnimeProcessingRoom apr, int epIdx)
        {
            List<ChapterAtom> chapters;
            List<string> lines = new List<string>();
            
            if (apr.EpisodeChapterData[epIdx] != null)
                chapters = apr.EpisodeChapterData[epIdx].Chapters;
            else
                return lines;

            string[] headers = new string[] { "UID", "START TIME", "END TIME", "LANG", "NAME" };
            int[] cols_len = new int[] { headers[0].Length, headers[1].Length, headers[2].Length, headers[3].Length, headers[4].Length };
            
            // Calculate column and row sizes
            for (int i = 0; i < chapters.Count; i++)
            {
                int lang_len = 0;
                int name_len = 0;
                foreach (ChapterDisplay cd in chapters[i].Displays)
                {
                    string lang = $"{cd.Language}:{cd.LanguageIETF}:{cd.Country}";       
                    if (lang.Length > lang_len)
                        lang_len = lang.Length;

                    if (cd.Name.Length > name_len)
                        name_len = cd.Name.Length;
                }
                int[] cells_len = new int[] {
                                                20,
                                                chapters[i].StartTime.ToString().Length,
                                                chapters[i].EndTime.ToString().Length,
                                                lang_len,
                                                name_len
                                            };
                for (int j = 0; j < cells_len.Length; j++)
                    cols_len[j] = cells_len[j] > cols_len[j] ? cells_len[j] : cols_len[j];
            }
            
            string col_sep = " | ";
            int Width = cols_len.Sum() + (col_sep.Length * (cols_len.Length - 1));
            string row_sep = string.Concat(Enumerable.Repeat("-", Width));

            // Add Source title to table
            lines.Add($"CHAPTERS");
            lines.Add(row_sep);
            if (apr.Settings.ChapterSourceIdx.HasValue && apr.Settings.ChapterSourceIdx > 0)
            {
                lines.Add($"Chapter Source Index: {apr.Settings.ChapterSourceIdx}");
                lines.Add($"Chapter Sync: {apr.Syncs[epIdx][(int)apr.Settings.ChapterSourceIdx - 1].ToString("+#;-#;0")}");
            }

            lines.Add($"Default Template Idx: {apr.ChapterEngineDefaultsTable[epIdx][0]}");
            lines.Add($"Default Override Idx: {apr.ChapterEngineDefaultsTable[epIdx][1]}");
            lines.Add(row_sep);
            
            // Create table header
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < headers.Length; i++)
            {
                sb.Append(headers[i] + string.Concat(Enumerable.Repeat(" ", cols_len[i] - headers[i].Length)));
                if (i < headers.Length - 1)
                    sb.Append(col_sep);
            }
            lines.Add(sb.ToString());
            lines.Add(row_sep);

            // Add rows to table
            for (int i = 0; i < chapters.Count; i++)
            {
                string uid = chapters[i].UID.ToString();
                int dsp_cnt = chapters[i].Displays.Count;
                string[] langs = new string[dsp_cnt];
                string[] names = new string[dsp_cnt];
                for (int j = 0; j < langs.Length; j++)
                {
                    ChapterDisplay cd = chapters[i].Displays[j];
                    langs[j] = $"{cd.Language}:{cd.LanguageIETF}:{cd.Country}";
                    names[j] = cd.Name;
                }

                string[][] cells = new string[dsp_cnt][];
                for (int j = 0; j < dsp_cnt; j++)
                {
                    if (j == 0)
                        cells[j] = new string[]
                        {
                            uid.PadLeft(cols_len[0]),
                            chapters[i].StartTime.ToString(),
                            chapters[i].EndTime.ToString(),
                            langs[j],
                            names[j]
                        };
                    else
                        cells[j] = new string[] {"", "", "", langs[j], names[j] };
                }

                
                foreach (string[] c in cells)
                {
                    sb = new StringBuilder();
                    for (int j = 0; j < headers.Length; j++)
                    {
                        sb.Append(c[j].PadRight(cols_len[j]));
                        if (j < headers.Length - 1)
                            sb.Append(col_sep);
                        
                        string derp = sb.ToString();
                    }

                    lines.Add(sb.ToString());
                }
                    
            }

            return lines;
        }
        
        public static List<string> Usage()
        {
            List<string> usage = new List<string>
            { 
                "Usage: anime-processing-room --dest <dest-dir> --name <show-name> --season <season-no.> --epMin <starting-ep-no.> --epMax <ending-ep-no.> <optional-args>", ""
            };
            usage.Add("Required arguments:");
            usage.Add("    --dest <arg>                 Destination directory for output files.");
            usage.Add("    --ep-min <arg>               Episode No. to start batch file build.");
            usage.Add("    --ep-max <arg>               Episode No. to finish batch file build.");
            usage.Add("");
            usage.Add("Optional arguments:");
            usage.Add("    --name <arg>                 Show/season name. Not required but recommended for tv episodes.");
            usage.Add("    --season <arg>               Season No. Not required but recommended for tv episodes.");
            usage.Add("                                 Default is '0'.");
            usage.Add("    --skip <arg>                 Comma-separated list of episode numbers to skip and not build.");
            usage.Add("    --ep-off  <arg>              Offset no. after which to start episode no. counter.");
            usage.Add("                                 Default is 'ep-min value - 1'.");
            usage.Add("    --chapter-source-index <arg> Index No. of which Source to use as the base for Chapter Engine.");
            usage.Add("                                 Defaults to the first Source File that is found.");
            usage.Add("    --ep-title-format <arg>      Text string specifying the format for the output mkv's internal title.");
            usage.Add("                                 See \"Docs/EpisodeTitleFormat.md\" for more details.");
            usage.Add("                                 Default is '{n} #{e} {t}'.");
            usage.Add("    --src-file-pref-format <arg> Text string specifying the format for the input source file's filenames (WITHOUT the file extension).");
            usage.Add("                                 See \"Docs/EpisodeTitleFormat.md\" for more details.");
            usage.Add("                                 Default is 'S{s|p:2}{e|p:2}'.");
            usage.Add("    --dest-filename-format <arg> Text string specifying the format for the output mkv's internal title.");
            usage.Add("                                 See \"Docs/EpisodeTitleFormat.md\" for more details.");
            usage.Add("                                 Default is 'S{s|p:2}E{e|p:2} - {t}.mkv'.");
            usage.Add("    --mkvmerge <arg>             Custom override path to 'mkvmerge' app executable");
            usage.Add("    --mkvinfo <arg>              Custom override path to 'mkvinfo' app executable");
            usage.Add("    --mkvextract <arg>           Custom override path to 'mkvextract' app executable");
            usage.Add("    --meta-dir <arg>             Custom override path to base directory containing the necessary metadata files.");
            usage.Add("                                 Is overridden by other file path override options.");
            usage.Add("    --source-base-dir <arg>      Custom override path to base directory containing the source content dirs/files.");
            usage.Add("                                 Used as base dir for source field when loading sources.csv.");
            usage.Add("    --sources-file <arg>         Custom override path to sources.csv metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --titles-file <arg>          Custom override path to titles.txt metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --trim-file <arg>            Custom override path to trim-times.txt metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --torders-file <arg>         Custom override path to track-orders.txt metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --tst-file <arg>             Custom override path to track-source-table.csv metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --cedt-file <arg>            Custom override path to ce-defaults-table.csv metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --dtt-file <arg>             Custom override path to dyn-track-titles.csv metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --syncs-file <arg>           Custom override path to sync-times.csv metadata file. Takes priority over '--meta-dir'.");
            usage.Add("    --chapters-dir <arg>         Custom override path to base directory containing the necessary chapter metadata files.");
            usage.Add("                                 See \"Docs/Chapters.md\" for more details.");
            usage.Add("    --verbose                    Output final generated raw command passed to mkvmerge");
            usage.Add("    --simulate                   Output final generated raw command passed to mkvmerge, but do not actually run mkvmerge or build anything.");
            usage.Add("    --overwrite                  Overwrite output files already existing in destination directory instead of skipping them");
            usage.Add("    --help                       Display this message and exit program.");

            return usage;
        }
    }
}