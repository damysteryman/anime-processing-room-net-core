using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;


namespace anime_processing_room_net_core
{
    public class AnimeProcessingRoomSourceException : Exception
    {
        public AnimeProcessingRoomSourceException(string _message, Exception _innerException) : base(_message, _innerException) { }
    }

    public class Source
    {
        public string SrcPath { get; set; }
        public List<string> TrackMetaFiles { get; set; }
        public List<List<Track>> Tracks { get; set; }

        public Source(string srcBaseDir, string metaBaseDir, string[] pts)
        {
            if (pts.Length < 1)
                throw new AnimeProcessingRoomSourceException("SOURCE EXCEPTION: Current Source entry is empty.", null);
            else if (pts.Length < 2)
                throw new AnimeProcessingRoomSourceException("SOURCE EXCEPTION: Current Source entry is missing Metadata CSV path.", null);
            else
            {
                if (string.IsNullOrEmpty(pts[0]))
                    throw new AnimeProcessingRoomSourceException("SOURCE EXCEPTION: Source file path is empty.", null);
                else if (string.IsNullOrEmpty(pts[1]))
                    throw new AnimeProcessingRoomSourceException("SOURCE EXCEPTION: Source's 1st Track List Meta CSV path is empty.", null);
            }

            TrackMetaFiles = new List<string>();
            Tracks = new List<List<Track>>();
            SrcPath = Path.Join(srcBaseDir, pts[0]);
            TrackMetaFiles = pts
                            .Skip(1)
                            .ToList();

            for (int i = 1; i < pts.Length; i++)
            {
                if (!string.IsNullOrEmpty(pts[i]))
                    Tracks.Add(GetTrackListMeta(Path.Join(metaBaseDir, pts[i])));
            }
        }

        private List<Track> GetTrackListMeta(string _trackMetaPath)
        {
            try
            {
                // Load Track List Metadata from file
                List<Track> tracks = new List<Track>();
                string[][] trackPts = IO.ParseStringCSV(_trackMetaPath, false, false, true, true);
                foreach (string[] l in trackPts)
                    tracks.Add(l == null ? null : new Track(l));
                    
                // If a Track does not have source name data, copy it from previous track
                for (int i = 0; i < tracks.Count; i++)
                    if (tracks[i] != null)
                    {
                        int count = i;
                        while (string.IsNullOrEmpty(tracks[i].Source) && count > 0)
                        {
                            if (tracks[count - 1] != null)
                                tracks[i].Source = tracks[count - 1].Source;
                            count--;
                        }
                    }
                return tracks;
            }
            catch (Exception e)
            {
                throw new AnimeProcessingRoomSourceException($"SOURCE EXCEPTION: Cannot load Track Metadata CSV file: {_trackMetaPath}", e);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"PATH: {SrcPath}");
            for (int i = 0; i < TrackMetaFiles.Count; i++)
            {
                sb.AppendLine($"FILE: {TrackMetaFiles[i]}");
                foreach (Track t in Tracks[i])
                    sb.Append(t.ToString());
            }
            sb.AppendLine();

            return sb.ToString();
        }
    }
}