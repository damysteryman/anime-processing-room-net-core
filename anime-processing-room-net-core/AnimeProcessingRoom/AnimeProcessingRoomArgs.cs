using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace anime_processing_room_net_core
{
    public class AnimeProcessingRoomArgs
    {
        private int _epMin;
        private int _epMax;
        
        public string Destination { get; set; }
        public int EpisodeMin { get { return _epMin; } set { _epMin = value > 0 ? value : 1; } }
        public int EpisodeMax { get { return _epMax; } set { _epMax = value > 0 ? value : 1; } }
        public int EpisodeCount { get { return EpisodeMax - EpisodeMin + 1; } }
        public string Name { get; set; }
        public int Season { get; set; }
        public List<int> SkipEpisodes { get; set; }
        public int? EpisodeOffset { get; set; }
        public int? ChapterSourceIdx { get; set; }
        public string EpisodeTitleFormat { get; set; }
        public string SrcFilePrefixFormat { get; set; }
        public string DestFilenameFormat { get; set; }
        public int EpisodeFileNumberDigits { get; set; }
        public string[] MTNToolPaths { get; set; }
        public string MetaFilesDirPath { get; set; }
        public string SourcesContentBasePath { get; set; }
        public string SourcesMetaFilePath { get; set; }
        public string TitlesFilePath { get; set; }
        public string TrimFilePath { get; set; }
        public string TrackOrdersFilePath { get; set; }
        public string TrackSourceTableFilePath { get; set; }
        public string ChapterEngineDefaultsTableFilePath { get; set; }
        public string DynamicTrackTitlesFilePath { get; set; }
        public string SyncTimesFilePath { get; set; }
        public string ChapterMetaDirPath { get; set; }
        public bool GenerateCrc32 { get { return Regex.IsMatch(DestFilenameFormat, ".*{(c|crc32)}.*"); } }

        public AnimeProcessingRoomArgs
        (
            string _dest,
            int _epMin,
            int _epMax,
            string _name = "",
            int _season = 0,
            List<int> _skipEps = null,
            int? _epOff = null,
            int? _chapterSourceIdx = 0,
            string _epTitleFormat = "",
            string _srcFilePrefixFormat = "",
            string _destFilenameFormat = "",
            string _metaDir = "",
            string _srcBase = "",
            string _sourcesFile = "",
            string _titlesFile = "",
            string _trimFile = "",
            string _tOrdersFile = "",
            string _tstFile = "",
            string _cedtFile = "",
            string _dttFile = "",
            string _syncsFile = "",
            string _chapterDir = "",
            string[] _mtntoolpaths = null
        )
        {
            Destination = _dest;
            EpisodeMin = _epMin;
            EpisodeMax = _epMax;
            Name = _name;
            Season = _season;
            SkipEpisodes = _skipEps == null ? new List<int>() : _skipEps;
            EpisodeOffset = _epOff.HasValue ? _epOff : EpisodeMin - 1;
            ChapterSourceIdx = _chapterSourceIdx;
            EpisodeTitleFormat = string.IsNullOrEmpty(_epTitleFormat) ? "{n} #{e} {t}" : _epTitleFormat;
            SrcFilePrefixFormat = string.IsNullOrEmpty(_srcFilePrefixFormat) ? "S{s|p:2}E{e|p:2}" : _srcFilePrefixFormat;
            DestFilenameFormat = string.IsNullOrEmpty(_destFilenameFormat) ? "S{s|p:2}E{e|p:2} - {t}.mkv" : _destFilenameFormat;
            MetaFilesDirPath = string.IsNullOrEmpty(_metaDir) ? Path.Join("meta", Season.ToString()) : _metaDir;
            SourcesContentBasePath = string.IsNullOrEmpty(_srcBase) ? "src" : _srcBase;
            SourcesMetaFilePath = string.IsNullOrEmpty(_sourcesFile) ? Path.Join(MetaFilesDirPath, "sources.csv") : _sourcesFile;
            TitlesFilePath = string.IsNullOrEmpty(_titlesFile) ? Path.Join(MetaFilesDirPath, "titles.txt") : _titlesFile;
            TrimFilePath = string.IsNullOrEmpty(_trimFile) ? Path.Join(MetaFilesDirPath, "trim-times.txt") : _trimFile;
            TrackOrdersFilePath = string.IsNullOrEmpty(_tOrdersFile) ? Path.Join(MetaFilesDirPath, "track-orders.txt") : _tOrdersFile;
            TrackSourceTableFilePath = string.IsNullOrEmpty(_tstFile) ? Path.Join(MetaFilesDirPath, "track-source-table.csv") : _tstFile;
            ChapterEngineDefaultsTableFilePath = string.IsNullOrEmpty(_cedtFile) ? Path.Join(MetaFilesDirPath, "ce-defaults-table.csv") : _cedtFile;
            DynamicTrackTitlesFilePath = string.IsNullOrEmpty(_dttFile) ? Path.Join(MetaFilesDirPath, "dyn-track-titles.csv") : _dttFile;
            SyncTimesFilePath = string.IsNullOrEmpty(_syncsFile) ? Path.Join(MetaFilesDirPath, "sync-times.csv") : _syncsFile;
            ChapterMetaDirPath = string.IsNullOrEmpty(_chapterDir) ? Path.Join(MetaFilesDirPath, "chapters") : _chapterDir;
            MTNToolPaths = _mtntoolpaths != null ? _mtntoolpaths : new string[] { "", "", "" };
        }
    }
}