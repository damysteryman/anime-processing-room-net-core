using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace anime_processing_room_net_core
{
    public class AnimeProcessingRoomException : Exception
    {
        public AnimeProcessingRoomException(string _message, Exception _innerException) : base(_message, _innerException) { }
    }

    public class AnimeProcessingRoom
    {
        #region Event Handler

        public event EventHandler<MsgArgs> OnMessage;

        #endregion
        
        #region Properties

        public AnimeProcessingRoomArgs Settings { get; private set; }
        public List<Source> Sources { get; private set; }
        public string[] Titles { get; private set; }
        public string[] TrimTimes { get; private set; }
        public string[] TrackOrders { get; private set; }
        public int[][] Syncs { get; private set; }
        public int[][] TrackSourceTable { get; private set; }
        public int[][] ChapterEngineDefaultsTable { get; private set; }
        public string[][] DynTrackTitles { get; private set; }
        public List<string>[] EpisodeProcessArgs { get; private set; }
        public string[] EpisodeFullTitles { get; private set; }
        public string[] EpisodeFileNames { get; private set; }
        public int[] EpisodeNumbers { get; private set; }
        public Edition[] EpisodeChapterData { get; private set; }
        public bool[][] EpisodeEnabledSources { get; private set; }
        
        #endregion

        #region Vars

        private MkvMergeProcess MKP;
        private int FirstFoundSrcIdx { get; set; }
        private string[] srcFileExts = new string[] { ".mkv", ".mka", ".mks", ".temp" };

        #endregion

        #region Constructor

        public AnimeProcessingRoom(AnimeProcessingRoomArgs args)
        {
            Settings = args;
        }

        #endregion

        #region Methods
        
        #region private

        private List<string> PrepTrackArgs(int _sindex, List<Track> _tracks, int _sync, string[] _dyntracktitles, bool _forceIncludeAll = false)
        {
            List<string> args = new List<string>();
            string video = "-D";
            string audio = "-A";
            string subtitle = "-S";
            List<int> vid = new List<int>();
            List<int> aud = new List<int>();
            List<int> sub = new List<int>();

            for (int i = 0; i < _tracks.Count; i++)
            {
                if (_tracks[i] != null)
                {
                    switch (_tracks[i].Type)
                    {
                        case Track.TYPE.VIDEO:
                            vid.Add(i);
                            break;

                        case Track.TYPE.AUDIO:
                            aud.Add(i);
                            break;

                        case Track.TYPE.SUBTITLE:
                            sub.Add(i);
                            break;
                    }

                    string name = _tracks[i].Name;
                    string source = _tracks[i].Source;

                    if (_dyntracktitles != null)
                        for (int j = 0; j < _dyntracktitles.Length; j++)
                        {
                            name = name.Replace($"{{{j}}}", _dyntracktitles[j]);
                            source = source.Replace($"{{{j}}}", _dyntracktitles[j]);
                        }

                    args.Add(
                        $"--language {i}:{_tracks[i].Language} " +
                        $"--track-name \"{i}:{name} [{source}]\" " +
                        $"-y {i}:{_sync} " +
                        $"--default-track {i}:{(_tracks[i].IsDefault ? "yes" : "no")} " +
                        $"--forced-track {i}:{(_tracks[i].IsForced ? "yes" : "no")} " +
                        $"--hearing-impaired-flag {i}:{(_tracks[i].IsHearingImpaired ? "yes" : "no")} " +
                        $"--visual-impaired-flag {i}:{(_tracks[i].IsVisualImpaired ? "yes" : "no")} "  +
                        $"--text-descriptions-flag {i}:{(_tracks[i].IsTextDescriptions ? "yes" : "no")} " +
                        $"--original-flag {i}:{(_tracks[i].IsOriginalLang ? "yes" : "no")} " +
                        $"--commentary-flag {i}:{(_tracks[i].IsCommentary ? "yes" : "no")}"
                    );
                }
            }

            if (_forceIncludeAll)
            {
                video = "";
                audio = "";
                subtitle = "";
            }
            else
            {
                if (vid.Count > 0)
                    video = $"-d {string.Join(",", vid)}";
                if (aud.Count > 0)
                    audio = $"-a {string.Join(",", aud)}";
                if (sub.Count > 0)
                    subtitle = $"-s {string.Join(",", sub)}";
            }

            return new List<string> { video, audio, subtitle }.Concat(args).ToList();
        }
        
        private List<string> PrepAttArgs(string _attDir)
        {
            List<string> attArgs = new List<string>();
            if (Directory.Exists(_attDir))
            {
                string[] atts = Directory.GetFiles(_attDir);
                foreach (string a in atts)
                {
                    string mimeType = "";
                    switch (Path.GetExtension(a).ToLower())
                    {
                        case ".ttf":
                        case ".ttc":
                            mimeType = "application/x-truetype-font";
                            break;

                        case ".otf":
                            mimeType = "application/vnd.ms-opentype";
                            break;

                        case ".jpg":
                            mimeType = "image/jpeg";
                            break;

                        case ".png":
                            mimeType = "image/png";
                            break;

                        default:
                            mimeType = "application/octet-stream";
                            break;
                    }
                    attArgs.Add($"--attachment-name \"{Path.GetFileName(a)}\" --attachment-mime-type {mimeType} --attach-file \"{a}\"");
                }
            }
            return attArgs;
        }

        private string PrepChapterArgs(int epIdx, string filePath, int sync = 0)
        {
            string filePrefix = GetFormattedTitle(epIdx, Settings.SrcFilePrefixFormat);

            // Setup Chapter args
            string chp = "";
            string chpFile = Path.Join(Settings.ChapterMetaDirPath, "raw", $"{filePrefix}.xml");
            if (File.Exists(chpFile))
                chp = $"--no-chapters --chapter-charset UTF-8 --chapter-sync {sync} --chapters \"{chpFile}\"";
            else
            {
                string dynChpPath = Path.Join(Settings.ChapterMetaDirPath, "dyn");
                ChapterEngine ce = new ChapterEngine($"{filePrefix}", filePath, dynChpPath);

                if(ce.Process(ChapterEngineDefaultsTable[epIdx][0], ChapterEngineDefaultsTable[epIdx][1]))
                {
                    EpisodeChapterData[epIdx] = ce.Editions[0];
                    string tmpChp = Path.Join(Path.GetTempPath(), $"APR-{filePrefix}-chp.xml");
                    File.WriteAllText(tmpChp, ce.ExportToXML(), Encoding.UTF8);
                    chp = $"--no-chapters --chapter-charset UTF-8 --chapter-sync {sync} --chapters \"{tmpChp}\"";
                }
            }

            return chp;
        }

        private bool GetAllMTNPaths()
        {
            string[] mtnTools = new string[] { "mkvmerge", "mkvinfo", "mkvextract" };
            
            for ( int i = 0; i < Settings.MTNToolPaths.Length; i++)
            {
                OnMessage(this, new MsgArgs($"Looking for '{mtnTools[i]}' app on system... "));
                try
                {
                    Settings.MTNToolPaths[i] = Mkvmerge.GetPath(mtnTools[i], Settings.MTNToolPaths[i]);
                    OnMessage(this, new MsgArgs($"OK! [{Settings.MTNToolPaths[i]}]"));
                }
                catch (Exception e)
                {
                    OnMessage(this, new MsgArgs(e.Message));
                    return false;
                }
            }
            return true;
        }

        private List<Source> GetAllSources(string _path)
        {
            List<Source> sources = new List<Source>();

            // Load sources from metadata files
            string[][] sourcePts = IO.ParseStringCSV(_path, true, false, false, true);
            foreach (string[] _pts in sourcePts)
                sources.Add(new Source(Settings.SourcesContentBasePath, Settings.MetaFilesDirPath, _pts));

            // Set all track's IDs
            for (int i = 0; i < sources.Count; i++)
                for (int j = 0; j < sources[i].Tracks.Count; j++)
                    for (int k = 0; k < sources[i].Tracks[j].Count; k++)
                        if (sources[i].Tracks[j][k] != null)
                            sources[i].Tracks[j][k].ID = $"{i.ToString()}:{k.ToString()}";

            return sources;
        }

        private void SetTrackTypesFromFile(List<Track> tracks, string _filePath)
        {
            string output = Mkvmerge.RunMtnApp("mkvinfo", "\"" + _filePath + "\"");
            MatchCollection results = Regex.Matches(output, "Track type: (video|audio|subtitles)");

            for (int i = 0; i < results.Count; i++)
            {
                Track.TYPE type = Track.TYPE.NONE;
                switch (results[i].Value.Substring(12))
                {
                    case "video":
                        type = Track.TYPE.VIDEO;
                        break;

                    case "audio":
                        type = Track.TYPE.AUDIO;
                        break;
                    
                    case "subtitles":
                        type = Track.TYPE.SUBTITLE;
                        break;
                }

                if (tracks.Count > i && tracks[i] != null)
                {
                    tracks[i].Type = type;
                }
            }
        }
        
        private string GetEpisodeTitle(int epIdx) => GetFormattedTitle(epIdx, Settings.EpisodeTitleFormat);

        private string GetFormattedTitle(int epIdx, string format, uint crc32 = 0)
        {
            string title = format;
            MatchCollection items = Regex.Matches(format, @"\{([^}]*)\}");

            foreach (Match itm in items)
            {
                string[] opts = itm.Value.Substring(1, itm.Value.Length - 2).Split('|');

                switch (opts[0])
                {
                    case "n":
                    case "name":
                        title = title.Replace(itm.Value, Settings.Name);
                        break;

                    case "s":
                    case "season":
                        title = title.Replace(itm.Value, FormatNumber(Settings.Season, opts));
                        break;

                    case "e":
                    case "episode":
                        title = title.Replace(itm.Value, FormatNumber(EpisodeNumbers[epIdx], opts));
                        break;

                    case "t":
                    case "title":
                        if (Titles != null)
                            title = title.Replace(itm.Value, Titles[epIdx]);
                        else
                            OnMessage(this, new MsgArgs($"WARNING: Title Format Item Type 't' specified in Title Format Item: '{itm.Value}', but Titles were not supplied, ignoring it."));
                        break;

                    case "c":
                    case "crc32":
                        if (crc32 > 0)
                            title = title.Replace(itm.Value, crc32.ToString("X8"));
                        break;

                    default:
                        OnMessage(this, new MsgArgs($"WARNING: Title Format Item Type '{opts[0]}' in Title Format Item: '{itm.Value}' is not recognized, ignoring it."));
                        break;
                }
            }
            
            return title;
        }

        private string FormatNumber(int value, string[] opts)
        {
            bool roman = false;
            bool ordinal = false;
            bool numberWord = false;
            int padDigits = 0;
            int offset = 0;

            for (int i = 1; i < opts.Length; i++)
            {
                string[] opt = opts[i].Split(':');
                switch (opt[0])
                {
                    case "f":
                        foreach (char o in opt[1])
                        {
                            switch (o)
                            {
                                case 'r':
                                    roman = true;
                                    break;

                                case 'o':
                                    ordinal = true;
                                    break;

                                case 'w':
                                    numberWord = true;
                                    break;

                                default:
                                    OnMessage(this, new MsgArgs($"WARNING: Format '{o}' in Title Format Option: '{opts[i]}' is not recognized, ignoring it."));
                                    break;
                            }
                        }
                        break;

                    case "o":
                        if (!int.TryParse(opt[1], out offset))
                            OnMessage(this, new MsgArgs($"WARNING: Number Offset Amount '{opt[1]}' in Title Number Offset Option: '{opts[i]}' is not recognized as a number, ignoring it."));
                        break;

                    case "p":
                        if (!int.TryParse(opt[1], out padDigits))
                            OnMessage(this, new MsgArgs($"WARNING: Number Padding Amount '{opt[1]}' in Title Number Padding Option: '{opts[i]}' is not recognized as a number, ignoring it."));
                        break;
                }
            }

            int number = value + offset;
            if (roman)
                return number.ToRoman();
            else if (ordinal && numberWord)
                return number.NumberWord(true);
            else if (ordinal)
                return number.Ordinal();
            else if (numberWord)
                return number.NumberWord();
            else
                return number.PadNumber(padDigits);
        }

        private string GetEpisodeFileName(int epIdx, uint crc32 = 0)
        {
            string badChars = new string(Path.GetInvalidFileNameChars()) + ":?<>|*/\\\"";
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(badChars)));

            return r.Replace(GetFormattedTitle(epIdx, Settings.DestFilenameFormat, crc32), "_");
        }

        private bool ProcessSource(int srcIdx, int epIdx)
        {
            string sbp = Sources[srcIdx].SrcPath;
            string filePrefix = GetFormattedTitle(epIdx, Settings.SrcFilePrefixFormat);
            string filePathNoExt = Path.Join(sbp, filePrefix);
            List<string> srcFiles;
            try
            {
                srcFiles = Directory.GetFiles(Sources[srcIdx].SrcPath, $"{filePrefix}*").ToList();
            }
            catch (Exception)
            {
                OnMessage(this, new MsgArgs($"WARNING: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: Source No.{srcIdx + 1}: Failed to load {sbp} source base dir, skipping this source for this episode..."));
                return false;
            }
            srcFiles.Sort();

            string filePath = "";
            if (srcFiles.Count > 0)
            {
                bool srcIsMatroska = srcFileExts.Contains(Path.GetExtension(srcFiles[0]));
                if (srcFiles.Count == 1 && srcIsMatroska)
                    filePath = srcFiles[0];
                else if (srcFiles.Count > 1 || !srcIsMatroska)
                    filePath = MakeTempSrcFile(epIdx, srcIdx, srcFiles, filePathNoExt);
            }

            if (!string.IsNullOrEmpty(filePath))
            {
                SetTrackTypesFromFile(Sources[srcIdx].Tracks[TrackSourceTable[epIdx][srcIdx]], filePath);

                // If this source is actually found, set source idx var for chapter args
                if (FirstFoundSrcIdx < 0)
                    FirstFoundSrcIdx = srcIdx;

                // Add attachment args if not from a .temp file
                // .temp files are assumed to already have attachments built in, and thus will be automatically handled by mkvmerge app
                if (!filePath.EndsWith(".temp"))
                    EpisodeProcessArgs[epIdx].AddRange(PrepAttArgs($"{filePathNoExt}_attachments"));

                // Add Chapter args
                // By default, use chapters from first found source file index
                // But if Chapter Source Index is explicitly specified then try to use that source index for chapters
                if (Settings.ChapterSourceIdx.HasValue)
                    EpisodeProcessArgs[epIdx].Add(srcIdx == Settings.ChapterSourceIdx ? PrepChapterArgs(epIdx, filePath, srcIdx > 0 ? Syncs[epIdx][srcIdx - 1] : 0) : "--no-chapters");
                else
                    EpisodeProcessArgs[epIdx].Add(srcIdx == FirstFoundSrcIdx ? PrepChapterArgs(epIdx, filePath, 0) : "--no-chapters");

                // Add track args
                EpisodeProcessArgs[epIdx].AddRange(PrepTrackArgs(srcIdx, Sources[srcIdx].Tracks[TrackSourceTable[epIdx][srcIdx]], srcIdx > 0 ? Syncs[epIdx][srcIdx - 1] : 0, DynTrackTitles[epIdx]));
                
                // Add input file arg for source
                EpisodeProcessArgs[epIdx].Add($"\"(\" \"{filePath}\" \")\"");

                return true;
            }
            else
            {
                OnMessage(this, new MsgArgs($"WARNING: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: Source No.{srcIdx + 1}: {filePathNoExt + ".*"} source file not found, skipping this source for this episode..."));
                return false;
            }
        }

        private string MakeTempSrcFile(int epIdx, int srcIdx, List<string> srcFiles, string filePathNoExt)
        {
            srcFiles.RemoveAll(s => s.EndsWith(".temp"));
            // create .temp mkv if source data is not in .mk* container
            string destFile = $"{filePathNoExt}.temp";
            Console.WriteLine($"Creating {destFile} for Source No.{srcIdx + 1}...");
            List<string> cmd = new List<string>();

            cmd.Add($"-o \"{destFile}\"");                              // set dest .temp file
            cmd.AddRange(PrepAttArgs($"{filePathNoExt}_attachments"));  // add attachments if necessary

            int trackCount = 0;
            foreach (string f in srcFiles)
            {
                if (f.EndsWith(".mp4"))
                {
                    cmd.AddRange(PrepTrackArgs(srcIdx, new List<Track>() {
                                                                    Sources[srcIdx].Tracks[TrackSourceTable[epIdx][srcIdx]][trackCount],
                                                                    Sources[srcIdx].Tracks[TrackSourceTable[epIdx][srcIdx]][trackCount+1]
                                                                }, 0, DynTrackTitles[epIdx], true));
                    trackCount++;
                }
                else
                {
                    cmd.AddRange(PrepTrackArgs(srcIdx, new List<Track>() { Sources[srcIdx].Tracks[TrackSourceTable[epIdx][srcIdx]][trackCount] }, 0, DynTrackTitles[epIdx], true));
                }
                
                cmd.Add($"\"(\" \"{f}\" \")\"");
                trackCount++;
            }

            // create .temp file and update source filePath with new .temp file path
            MKP.Run(cmd);                           

            return File.Exists(destFile) ? destFile : "";
        }

        private bool AddCrc32ToFilename(int epIdx)
        {
            string fullDestPath = Path.Join(Settings.Destination, EpisodeFileNames[epIdx]);
            if (Settings.GenerateCrc32 && File.Exists(fullDestPath))
            {
                OnMessage(this, new MsgArgs($"INFO: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: Generating CRC32..."));
                uint crc32 = Utils.GetCrc32(fullDestPath);

                string newName = GetEpisodeFileName(epIdx, crc32);
                OnMessage(this, new MsgArgs($"INFO: Renaming '{EpisodeFileNames[epIdx]}' to '{newName}'..."));
                File.Move(fullDestPath, Path.Join(Settings.Destination, newName));

                return true;
            }
            else
                return false;
        }
    
        #endregion

        #region public

        public bool LoadData()
        {
            // Check for MkvToolNix required tools before proceeding
            if (!GetAllMTNPaths())
            {
                OnMessage(this, new MsgArgs($"ERROR: MkvToolNix tools could not be found on this system. Cannot continue."));
                return false;
            }

            // Init MkvMergeProcess runner
            MKP = new MkvMergeProcess(Settings.MTNToolPaths[0]);
            MKP.OnMessage += OnMessage;

            // Check if specified sources.csv metadata file exists
            if (!File.Exists(Settings.SourcesMetaFilePath))
            {
                OnMessage(this, new MsgArgs($"ERROR: Source Meta File: \"{Settings.SourcesMetaFilePath}\" not found! Unable to continue."));
                return false;
            }

            // Load sources.csv metadata
            Sources = GetAllSources($"{Settings.SourcesMetaFilePath}");

            // Load titles.txt metadata
            if (File.Exists(Settings.TitlesFilePath))
                Titles = IO.ParseTXT(Settings.TitlesFilePath, Settings.EpisodeOffset.Value, Settings.EpisodeCount);

            // Load trim-times.txt metadata
            TrimTimes = IO.ParseTXT(Settings.TrimFilePath, Settings.EpisodeOffset.Value, Settings.EpisodeCount);
            for (int i = 0; i < TrimTimes.Length; i++)
                TrimTimes[i] = TrimTimes[i].Replace(',', '.');

            // Load track-orders.txt metadata
            TrackOrders = File.Exists(Settings.TrackOrdersFilePath) ? IO.ParseTXT(Settings.TrackOrdersFilePath, Settings.EpisodeOffset.Value, Settings.EpisodeCount) : null;
            if (TrackOrders != null)
            {
                if (string.IsNullOrEmpty(TrackOrders[0]))
                    TrackOrders[0] = "0:0";
                for (int i = 0; i < TrackOrders.Length; i++)
                    if (string.IsNullOrEmpty(TrackOrders[i]) && i > 0)
                        TrackOrders[i] = TrackOrders[i - 1];
            }                
            
            // Load sync-times.csv metadata
            Syncs = IO.ParseIntCSV(Settings.SyncTimesFilePath, Settings.EpisodeOffset.Value, Settings.EpisodeCount, Sources.Count);

            // Load track-source-table.csv metadata
            TrackSourceTable = IO.ParseIntCSV(Settings.TrackSourceTableFilePath, Settings.EpisodeOffset.Value, Settings.EpisodeCount, Sources.Count);

            // Load ce-defaults-table.csv metadata
            ChapterEngineDefaultsTable = IO.ParseIntCSV(Settings.ChapterEngineDefaultsTableFilePath, Settings.EpisodeOffset.Value, Settings.EpisodeCount, 2);

            // Load dyn-track-titles.csv metadata
            DynTrackTitles = IO.ParseStringCSV(Settings.DynamicTrackTitlesFilePath, false, true, true, false, Settings.EpisodeOffset.Value, Settings.EpisodeCount, 1);

            // Init collections using calculated Episode Count as size
            EpisodeFullTitles = new string[Settings.EpisodeCount];
            EpisodeFileNames = new string[Settings.EpisodeCount];
            EpisodeNumbers = new int[Settings.EpisodeCount];
            EpisodeChapterData = new Edition[Settings.EpisodeCount];
            EpisodeProcessArgs = new List<string>[Settings.EpisodeCount];
            EpisodeEnabledSources = new bool[Settings.EpisodeCount][];
            return true;
        }

        public bool ProcessEpisode(int epIdx)
        {
            EpisodeNumbers[epIdx] = epIdx + 1 + Settings.EpisodeOffset.Value;

            if (Settings.SkipEpisodes.Contains(EpisodeNumbers[epIdx]))
            {
                OnMessage(this, new MsgArgs($"WARNING: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: Is set to be skipped, so skipping..."));
                return false;
            }

            EpisodeProcessArgs[epIdx] = new List<string>();

            // init first found source index for current ep, for use with chapter stuff later
            FirstFoundSrcIdx = -1;

            // FOR EACH SOURCE
            // Add all source + chapter args
            EpisodeEnabledSources[epIdx] = new bool[Sources.Count];
            for (int i = 0; i < Sources.Count; i++)
                EpisodeEnabledSources[epIdx][i] = ProcessSource(i, epIdx);
   
            if (EpisodeEnabledSources[epIdx].Aggregate((b1, b2) => b1 || b2))
            {
                EpisodeFullTitles[epIdx] = GetEpisodeTitle(epIdx);
                EpisodeFileNames[epIdx] = GetEpisodeFileName(epIdx);

                // Add output file arg
                string fullDestPath = Path.Join(Settings.Destination, EpisodeFileNames[epIdx]);
                EpisodeProcessArgs[epIdx].Add($"-o \"{fullDestPath}\"");

                // Add title arg
                EpisodeProcessArgs[epIdx].Add($"--title \"{EpisodeFullTitles[epIdx].Replace("\"","\\\"")}\"");

                // Add track order arg
                if (TrackOrders != null && !String.IsNullOrEmpty(TrackOrders[epIdx]))
                    EpisodeProcessArgs[epIdx].Add($"--track-order {TrackOrders[epIdx]}");

                // Add trim-time arg
                if (!String.IsNullOrEmpty(TrimTimes[epIdx]))
                    EpisodeProcessArgs[epIdx].Add($"--split parts:{TrimTimes[epIdx]}");

                return true;
            }
            else
            {
                OnMessage(this, new MsgArgs($"WARNING: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: No Source Files available for building this episode, skipping this episode..."));
                return false;
            }
        }

        public void BuildEpisode(int epIdx, bool Overwrite)
        {
            if (Settings.SkipEpisodes.Contains(EpisodeNumbers[epIdx]))
            {
                OnMessage(this, new MsgArgs($"WARNING: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: Is set to be skipped, so skipping..."));
                return;
            }

            if (Directory.Exists(Settings.Destination))
            {
                // string[] oldFiles = Directory.GetFiles(Settings.Destination, EpisodeFileNames[epIdx].Replace("{crc32}", "????????"));
                string[] oldFiles = Directory.GetFiles(Settings.Destination, Regex.Replace(EpisodeFileNames[epIdx], "{(c|crc32)}", "????????"));

                if (Overwrite)
                {
                    foreach (string f in oldFiles)
                    {
                        OnMessage(this, new MsgArgs($"INFO: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: File \"{f}\" already exists and Overwrite option is specified, deleting it..."));
                        File.Delete(f);
                    }
                        
                }
                else if (oldFiles.Length > 0)
                {
                    foreach (string f in oldFiles)
                        OnMessage(this, new MsgArgs($"WARNING: {GetFormattedTitle(epIdx, "S{s|p:2}E{e|p:2}")}: File \"{f}\" already exists and Overwrite option not specified, skipping file..."));

                    return;
                }
            }
            
            if (EpisodeProcessArgs[epIdx].Count > 0)
            {
                MKP.Run(EpisodeProcessArgs[epIdx]);
                AddCrc32ToFilename(epIdx);
            }
        }

        #endregion

        #endregion
    }
}