using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace anime_processing_room_net_core
{
    public class Track
    {
        public enum TYPE
        {
            NONE,
            VIDEO,
            AUDIO,
            SUBTITLE
        }

        public string ID { get; set; }
        public TYPE Type { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public bool[] Flags { get; private set; } = new bool[7];
        public bool IsDefault { get => Flags[0]; set => Flags[0] = value; }
        public bool IsForced { get => Flags[1]; set => Flags[1] = value; }
        public bool IsHearingImpaired { get => Flags[2]; set => Flags[2] = value; }
        public bool IsVisualImpaired { get => Flags[3]; set => Flags[3] = value; }
        public bool IsTextDescriptions { get => Flags[4]; set => Flags[4] = value; }
        public bool IsOriginalLang { get => Flags[5]; set => Flags[5] = value; }
        public bool IsCommentary { get => Flags[6]; set => Flags[6] = value; }
        

        public Track
        (
            string _id,
            TYPE _type,
            string _language,
            string _name,
            bool _isDefault,
            bool _isForced,
            bool _isHearingImpaired,
            bool _isVisualImpaired,
            bool _isTextDescriptions,
            bool _isOriginalLang,
            bool _isCommentary,
            string _source
        )
        {
            ID = _id;
            Type = _type;
            Language = _language;
            Name = _name;
            Source = _source;

            Flags = new bool[7];
            IsDefault = _isDefault;
            IsForced = _isForced;
            IsHearingImpaired = _isHearingImpaired;
            IsVisualImpaired = _isVisualImpaired;
            IsTextDescriptions = _isTextDescriptions;
            IsOriginalLang = _isOriginalLang;
            IsCommentary = _isCommentary;
        }
        
        public Track(string[] _trackPts)
        {
            Type = TYPE.NONE;            
            Language = _trackPts[0];
            Name = _trackPts[1];
            if (_trackPts.Length > 2)
            {
                Flags = new bool[7];
                IsDefault = _trackPts[2].Contains("d");
                IsForced = _trackPts[2].Contains("f");
                IsHearingImpaired = _trackPts[2].Contains("h");
                IsVisualImpaired = _trackPts[2].Contains("v");
                IsTextDescriptions = _trackPts[2].Contains("t");
                IsOriginalLang = _trackPts[2].Contains("o");
                IsCommentary = _trackPts[2].Contains("c");
            }
            if (_trackPts.Length > 3)
                Source = _trackPts[3];
        }

        public override string ToString()
        {
            return $"{ID}\t{Type}\t{Language}\t{Name}\t{Source}";
        }
    }
}
