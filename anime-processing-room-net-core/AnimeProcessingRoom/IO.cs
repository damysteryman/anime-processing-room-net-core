using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace anime_processing_room_net_core
{
    public class AnimeProcessingRoomIOException : Exception
    {
        public AnimeProcessingRoomIOException(string _message, Exception _innerException) : base(_message, _innerException) { }
    }

    public static class IO
    {
        public static string[] ParseTXT(string _path, int _startIdx, int _lineCount)
        {
            try
            {
                return File.ReadAllText(_path).Replace("\r\n", "\n").Split('\n').Skip(_startIdx).Take(_lineCount).ToArray();
            }
            catch (IOException)
            {
                return Enumerable.Repeat("", _lineCount).ToArray();
            }
        }

        public static string[][] ParseStringCSV(string _path, bool _convertDirSlashes, bool _continueOnFail, bool _jagged, bool _enableComments, int _startIdx = 0, int _lineCount = 0, int _minColumns = 0)
        {
            try
            {
                // Setup IEnumerable for Linq query chain in specific order for CSV parsing
                // Read the file
                // Manually split File.ReadAllText() output instead of File.ReadAllLines() to preserve last line if it is empty
                // IEnumerable<string> lines = File.ReadAllLines(_path);
                IEnumerable<string> lines = File.ReadAllText(_path)
                                            .Replace("\r\n", "\n")
                                            .Split('\n');

                // Before anything else, if comments are allowed then filter them out
                if (_enableComments)
                    lines = lines.Where(l => !(l.StartsWith("#") || l.StartsWith("//")));

                // Skip to line corresponding to desired start index/episode
                lines = lines.Skip(_startIdx);

                // If line count is specified only read that many lines
                if (_lineCount > 0)
                    lines = lines.Take(_lineCount);

                // If set to search for and handle file path dir slashes, do that here
                if (_convertDirSlashes)
                    lines = lines.Select(l => Regex.Replace(l, @"[/\\]", Path.DirectorySeparatorChar.ToString()));

                // Evaluate linq query and store it
                string[][] result = lines.Select(s => s.Split('\t'))
                                    .Select(s => s.All(s => string.IsNullOrEmpty(s)) ? null : s)
                                    .ToArray();

                // Ensure all sub arrays' length is either the specified min length,
                // or all the same length as longest sub array
                // depending on if if jagged is set or not
                int maxColLen = result
                                .Select(r => r != null ? r.Length : 0)
                                .OrderBy(r => r)
                                .Last();
                int columns = _jagged
                            ? _minColumns
                            : maxColLen > _minColumns
                                ? maxColLen
                                : _minColumns;

                for (int i = 0; i < result.Length; i++)
                {
                    if (result[i] == null)
                    {
                        if (columns > 0)
                            result[i] = Enumerable.Repeat("", columns).ToArray();
                    }
                    else if (result[i].Length < columns)
                    {
                        string[] newLine = Enumerable.Repeat("", columns).ToArray();
                        result[i].CopyTo(newLine, 0);
                        result[i] = newLine;
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                if (_continueOnFail)
                {
                    string[][] result = new string[_lineCount][];
                    for (int i = 0; i < result.Length; i++)
                        result[i] = Enumerable.Repeat("", _minColumns).ToArray();
                    return result;
                }
                else
                {
                    throw new AnimeProcessingRoomIOException($"IO EXCEPTION: Cannot load {_path}!", e);
                }
            }
        }

        public static int[][] ParseIntCSV(string _path, int _startIdx, int _lineCount, int _minColumns = 1)
        {
            string[][] strcsv = ParseStringCSV(_path, false, true, false, true, _startIdx, _lineCount, _minColumns);
            int[][] result = new int[strcsv.Length][];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new int[strcsv[0].Length];

                for (int j = 0; j < result[i].Length; j++)
                    try
                    {
                        result[i][j] = int.Parse(strcsv[i][j]);
                    }
                    catch (FormatException)
                    {
                        result[i][j] = 0;
                    }
            }

            return result;
        }
    }
}
