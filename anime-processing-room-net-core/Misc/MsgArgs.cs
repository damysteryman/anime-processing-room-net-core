using System;

namespace anime_processing_room_net_core
{
    public class MsgArgs : EventArgs
    {
        public string Message { get; private set; }
        public MsgArgs(string _msg) => Message = _msg;
    }
}