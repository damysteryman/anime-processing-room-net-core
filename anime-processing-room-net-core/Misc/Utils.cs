using System;
using System.IO;
using Force.Crc32;

namespace anime_processing_room_net_core
{
    public static class Utils
    {
        public static string PadNumber(this int num, int digits)
        {
            string str = num.ToString();
            while (str.Length < digits)
                str = "0" + str;

            return str;
        }

        // https://stackoverflow.com/questions/20156/is-there-an-easy-way-to-create-ordinals-in-c
        public static string Ordinal(this int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }

        // https://stackoverflow.com/questions/7040289/converting-integers-to-roman-numerals
        public static string ToRoman(this int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;            
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); 
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);            
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public static string NumberWord(this int num, bool ord = false)
        {
            string numStr = num.ToString();
            int numMaxIdx = numStr.Length - 1;
            string str = "";
            string[] singles = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
            string[] doubles = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
            string[] tens = { singles[0], doubles[0], "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
            string[] singleOrds = { singles[0], "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth" };
            bool teens = false;

            for (int i = numMaxIdx; i >= 0; i--)
            {
                int digit = (int)Char.GetNumericValue(numStr[numMaxIdx - i]);

                switch (i)
                {
                    case 0:
                        if (teens)
                        {
                            str = doubles[digit];
                            if (ord)
                            {
                                if (digit == 2)
                                    str = str.Replace("ve", "f");
                                str += "th";
                            }
                        }
                            
                        else if (string.IsNullOrEmpty(str))
                            str = $"{(ord ? singleOrds[digit] : singles[digit])}";
                        else if (digit != 0)
                            str = $"{str}-{(ord ? singleOrds[digit] : singles[digit])}";
                        else
                            str = $"{(ord ? str.Replace("ty", "tieth") : str)}";
                        break;

                    case 1:
                        if (string.IsNullOrEmpty(str))
                            if (digit == 1)
                                teens = true;
                            else if (digit != 0)
                                str = $"{tens[digit]}";
                        break;
                }
            }
            return str;
        }

        public static uint GetCrc32(string path)
        {
            using (BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                long pos = 0;
                long len = br.BaseStream.Length;
                int size = 4096;
                byte[] buf = new byte[size];
                uint result = 0;

                while (pos < len)
                {
                    buf = br.ReadBytes(size);
                    result = Crc32Algorithm.Append(result, buf);
                    pos += size;
                }
                
                return result;
            }
        }
    }
}
