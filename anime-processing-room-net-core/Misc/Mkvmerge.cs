using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;

namespace anime_processing_room_net_core
{
    public class MkvMergeProcess
    {
        public event EventHandler<MsgArgs> OnMessage;
        ProcessStartInfo settings;

        public MkvMergeProcess(string _mkvmergePath)
        {
            settings = new ProcessStartInfo
            {
                FileName = _mkvmergePath,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = false
            };
        }

        public void Run(List<string> _args)
        {
            Process p = new Process();
            p.StartInfo = settings;
            p.StartInfo.Arguments = string.Join(" ", _args);
            p.OutputDataReceived += new DataReceivedEventHandler((sender, e) => OnMessage(this, new MsgArgs(e.Data)));

            p.Start();    
            p.BeginOutputReadLine();
            p.WaitForExit();
            p.Close();
        }
    }
    public static class Mkvmerge
    {
        public static string GetPath(string _exe, string _path = "")
        {
            string[] mkvmergePaths =
            {
                _path,
                $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)}\MKVToolNix\{_exe}.exe",
                $@"{Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)}\MKVToolNix\{_exe}.exe",
                $@"{_exe}",
                $@"/usr/bin/{_exe}",
                $@"mkvtoolnix\{_exe}.exe",
            };

            foreach (string mp in mkvmergePaths)
                if (File.Exists(mp))
                {
                    _path = mp;
                    break;
                }

            if (File.Exists(_path))
            {
                return _path;
            }
            else
            {
                string errMsg = "ERROR!" +
                                $"Program '{_exe}' not found!" + Environment.NewLine +
                                Environment.NewLine +
                                $"Default locations for {_exe} are:" +
                                Environment.NewLine +
                                "WINDOWS:" +
                                $@"<Program Files>\MKVToolNix\{_exe}.exe" +
                                $@"<Program Files (x86)>\MKVToolNix\{_exe}.exe" +
                                $@"<Current Working Dir>\mkvtoolnix\{_exe}.exe" +
                                Environment.NewLine +
                                "LINUX:" +
                                $"/usr/bin/{_exe}" +
                                $"<Current Working Dir>/{_exe}" +
                                Environment.NewLine +
                                $"Custom path to {_exe} executable can be set with the --{_exe} argument." +
                                $"{_exe} is part of the MKVToolNix package, available at https://mkvtoolnix.download/downloads.html" +
                                Environment.NewLine;

                throw new FileNotFoundException(errMsg, _path);
            }
        }
    
        // public static void RunMkvMerge(List<string> _args)
        // {
        //     using (Process process = Process.Start(
        //         new ProcessStartInfo
        //         {
        //             FileName = "mkvmerge",
        //             Arguments = string.Join(" ", _args),
        //             UseShellExecute = true,
        //             RedirectStandardOutput = false,
        //             CreateNoWindow = false
        //         }))
        //         process.WaitForExit();
        // }

        public static string RunMtnApp(string exe, string args)
        {
            using (Process process = Process.Start(new ProcessStartInfo
                {
                    FileName = exe,
                    Arguments = args,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }))
                using (StreamReader reader = process.StandardOutput)
                    return reader.ReadToEnd();
        }
    }
}
