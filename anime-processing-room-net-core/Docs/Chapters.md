# MKV Chapter Editing Support

Anime processing Room can also help to set/edit Chapters in output MKVs during batch processing. There are 2 ways chapter data can be loaded/processed:

1. The "Traditional" or "Raw" method, muxing a full chapter .xml file into the output MKV with MkvMerge.

2. Using Anime Processing Room's Chapter Engine to mix chapter data from a source MKV or chapter.xml file, along with custom .csv files containing partial information.

## "Traditional/Raw" Method

This method is simple. All this does is take a normal chapter .xml file coresponding to an Episode and tell MKVMerge to mux it into the output MKV file.<br>
The chapter .xml file must be one supported by MkvMerge.

Name the file {Source Prefix}.xml and place it into the {chapterDir}/raw subdirectory.

The Default Source Prefix is:<br>
"S{s|p:2}E{e|p:2}" (See Docs/EpisodeTitleFormat.md for more information)

The Default Chapter directory is:<br>
"{current working dir}/{season no.}/chapters"

For Example, if using default settings, to provide a chapter .xml for Season 1 Episode 1, you would put it in:<br>
"{current working dir}/1/chapters/raw/S01E01.xml"

Anime Processing Room will see it and instruct the MkvMerge to add the chapter .xml to the output MKV for the episode.

## Chapter Engine Method

Chapter Engine can be used to dynamically load, override, or edit Chapter information in an output MKV file. This is done in a 3-tiered approach:

1. First, a Full set of data from either a chapter .xml file or custom .csv file, or a source MKV file itself, can be used as a base. This first base stage must be a valid full set of chapter information (That is, no partial information files like the custom .csv files used for overlays). If you are using a source MKV's chapter data as a base, that should already be valid unless your source MKV is corrupt somehow.

2. Secondly, partial data from custom .csv files can be overlayed over the base data. For example, overlaying a per-season template with names for chapters over the internal chapter data from the source.

3. Finally, a second overlay .csv can also be applied after that for more specific overrides, such as an episode-specific modification to chapter data after an initial, more generic overlay has been applied. For example, if an episode has an extra chapter, or chapters in different orders compared to the rest of the episodes in a season being processed.

### Chapter Engine Data File Loading Order/Priority

| Stage | 1st Priority Source | OR 2nd | OR 3rd | Or 4th |
| ------ | ------ | ------ | ------ | ------ |
| 1. BASE | Chapter Data from Source MKV | {Source Prefix}-base.xml | base.xml | base.csv |
| ------ | ------ | ------ | ------ | ------ |
| | forced-template.csv |
| 2. TEMPLATE | AND THEN |
| | {Source Prefix}-template.csv | default{N}-template.csv | default-template.csv |
| ------ | ------ | ------ | ------ | ------ |
| | forced-override.csv |
| 3. OVERRIDE | AND THEN |
| | {Source Prefix}-override.csv | default{N}-override.csv | default-override.csv |

These files are to be placed into the {chapterDir}/dyn subdirectory.

The Default Source Prefix is:<br>
"S{s|p:2}E{e|p:2}" (See Docs/EpisodeTitleFormat.md for more information)

The Default Chapter directory is:<br>
"{current working dir}/{season no.}/chapters"

For Example, if using default settings, to read the chapters from the source MKV file for Season 1 Episode 1, then apply a default batch-job-wide template, then apply an epsiode-specific override, your directory/files paths would be as follows:<br>
"{current working dir}/chapters/1/dyn/default-template.csv"<br>
"{current working dir}/chapters/1/dyn/S01E01-override.csv"

### Choosing From Multiple Alternate Default Templates/Overrides

Multiple alternate default template/override CSVs can be specified and used on a per-Episode basis. For default{N}-template.csv and default{N}-override.csv, the {N} is the numbered index of which default template/override you wish to use for an Episode, for example: default0, default1, default2, etc. To use and choose which index defaults to use for which episode, create the file "ce-defaults-table.csv" inside the current batch job's season directory (NOT the "chapters/dyn" directory).

The default path for "ce-defaults.table.csv" is:
{current working dir}/{season no.}/ce-defaults.table.csv

The format of ce-defaults.table.csv is a tab-delimited table with 2 columns, and as many rows as there are Episodes, where the first column specifies the index for default{N}-template.csv, and the second column specifies the index for default{N}-override.csv. AnimeProcessingRoom will load this data, and will pass the 2 indexes to ChapterEngine when processing each corresponding Episode.

### Chapter Engine Template/Override CSV Format

A template/override chapter CSV file is a tab-delimited .csv file storing a table of chapter data. Each row represents a chapter, and each column, separated by tabs, represents one of the chapter's properties. Basic structure is something like this:

| Chapter Engine Options | Start Timestamp | End Timestamp | Chapter Language 1 | Chapter Title 1 | ... | Chapter Language N | Chapter Title N |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| {Options, see below for info} | {1st Chapter's Data} | | | | | | |
| | {2nd Chapter's Data} | | | | | | |
| | 00:00:00.000000000 | 00:01:30.1234556789 | eng:en | OP | | | |
| ...etc.  | | | | | | | |

Column description is as follows:

1. Chapter Engine Options:

   This column is for specifying any special options for Chapter Engine to apply to this Chapter Entry. Normally this column is left empty, but there currently 2 supported options:

   | Option | Description |
   | ------ | ------ |
   | + | Adds/Inserts an extra chapter entry at the specified row in the MKV's chapter set. |
   | - | Removes/Deletes the existing chapter at the specified row in the MKV's chapter set. |

2. Start Timestamp:

   This is the timestamp for the start of the chapter. Example: 00:00:00.000000000

3. End Timestamp:

   This is the timestamp for the end of the chapter. Example: 00:24:02.023916666

4. Chapter Language:

   This is a string containing language/locale data for the Chapter's Name. It can contain 3 locale settings separated by a colon (':'):

   - Traditional 3-character ISO 639-2 language code (Example: eng for English)
   - IETF BCP 47 based language string (Example: en for English, or en-US for American English)
   - Country Code (Example: us for United States)


   If there is a locale setting that you do not wish to set, then just keep it blank (but keep the separator colons if the locale you are omitting is not the last AKA the Country setting).

   Full Chapter Language examples:

   | Language String | Desription |
   | ------ | ------|
   | jpn:ja:jp | Japanese (Japan) |
   | eng:en | English (No country specified) |
   | eng:en-US:us | American English (US) |

5. Chapter Title:

   This is the actual Name/Title of this Chapter Entry. Example: OP, Part A, Preview, etc.<br>
   It corresponds to the Chapter Language specified in the previous column.

Chapter Language and Chapter Title are treated as a set, that is, it is the Chapter Tile for that specified language. This pair of columns can also be repeated to add multiple titles to a Chapter.

If a Chapter Engine CSV file is used in the initial Base stage (base.csv), then all Start and End Timestamps for all Chapter Entries must be specified bare minimum, but specifying all data in all columns is recommended.

Otherwise, when using a Chapter Engine CSV for Template or Override stages, only the data you wish to override needs to be specified.

Additionally, when inserting new chapters with the + Option, if the chapters adjacent to it (either in the .csv or already loaded into Chapter Engine) already have valid timestamp data, it is possible to insert a new chapter without manually specifying the timestamps for it. In this case, Chapter Engine will take the adjacent End Timestamp from the previous chapter, and the adjacent Start Timestamp from the following chapter, in order to deduce the Timestamps for the newly inserted chapter.