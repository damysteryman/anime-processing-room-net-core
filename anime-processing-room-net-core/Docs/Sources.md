# Sources

## Sources CSV Format

Column description is as follows:

1. Source Files Path

2. TrackList1 CSV Path


| Source Files Path | TrackList 1 CSV Path | Track List 2 | ... | Track List N |
| ------ | ------ | ------ | ------ | |
| JP | 1/tracks-jp.csv | | | |
| EN | 1/tracks-en.csv | tracks-en-cm.csv | | |
| ...etc | | | | |

The TrackList CSV path can be repeated to reference multiple Track Lists and use different Source/TrackList combos on a per-episode basis, based on the data specified in a Track Source Table CSV.