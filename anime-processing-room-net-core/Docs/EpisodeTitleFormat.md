# Episode Title / Filename Format

Title Formats are a text-based skeleton with dynamic items/fields used to determine Episode Titles, Source Filenames and Destination Filenames for each episode being processed.

## Default Format for MKV Internal Episode Title:

{n} #{e} {t} 

## Default Format for Source Filename Prefix:

S{s|p:2}E{e|p:2}<br>
Note that the Source Filename Prefix DOES NOT contain the file extension.

## Default Format for Destination Filename:

S{s|p:2}E{e|p:2} - {t}.mkv<br>
Note that the Destination Filename DOES contain the file extension.

## Dynamic Items:

Dynamic items consist of a letter denoting which item it corresponds to, potentially followed by options, separated by a pipe character ('|').<br>
Each extra option consists of a letter denoting which option it is, followed by the value for that option, separated by a colon character (':').<br>
The complete items are placed within {curly braces}.

## Dynamic item format:

{item|option1:option1value|option2:option2value| ..... |optionN:optionNvalue}

### Recognized items:

| Item | Alternative | Description |
| ------ | ------ | ------ |
| n | name | Show/season name/title |
| s | season | Season number |
| e | episode | Episode number |
| t | title | Episode title |
| c | crc32 | CRC32 of output file (NOTE: Can only be used with Destination Filename Format) |

## Option format:

option:value

### Recognized options:

| Option | Description |
| ------ | ------ |
| f | Extra number format |
| o | Number Offset |
| p | Number padding |

### Extra number format:

| Value | Description |
| ------ | ------ |
| o | Ordinal number |
| r | Roman numeral |
| w | Number-word |

NOTE: o and w can be combined for Ordinal Number-word format.

Example: {e|f:r}<br>
... will turn Episode 7 into "Episode VII".

### Number Offset

The Number Offset Option is used to offset the number by a specified amount.<br>
For the value, specify the amount you wish to offset the number by.<br>

Example: {e|o:24}<br>
... will cause turn Episode 1 into Episode 25.

### Number Padding:

The Number Padding Option is used to pad numbers with leading zeros.<br>
For the value, specify the number of total digits number is meant to have.

Example: {e|p:3}<br>
...will make the Episode number 3 digits minimum; Episode 1 will turn into Episode 001.