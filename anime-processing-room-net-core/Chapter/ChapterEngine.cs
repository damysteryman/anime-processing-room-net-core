using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace anime_processing_room_net_core
{
    public class ChapterEngineException : Exception
    {
        public ChapterEngineException(string _message, Exception _innerException) : base(_message, _innerException) { }
    }

    public class ChapterEngine
    {
        private string _srcName;
        private string _srcPath;
        private string _chpPath;
        private readonly Random r = new Random();
        public List<Edition> Editions { get; set; }

        public ChapterEngine(string srcName, string srcPath, string chpDirPath)
        {
            _srcName = srcName;
            _srcPath = srcPath;
            _chpPath = chpDirPath;
        }

        public bool Process(int defaultTemplateIdx = 0, int defaultOverrideIdx = 0)
        {    
            string[][] chapterFiles = new string[][]
            {
                new string[] { _srcPath, $"{_srcName}-base.xml", "base.xml", "base.csv" },
                new string[] { "forced-template.csv" },
                new string[] { $"{_srcName}-template.csv", $"default{defaultTemplateIdx}-template.csv", "default-template.csv" },
                new string[] { "forced-override.csv" },
                new string[] { $"{_srcName}-override.csv", $"default{defaultOverrideIdx}-override.csv", "default-override.csv" }
            };

            Editions = new List<Edition>();

            for (int i = 0; i < chapterFiles.Length; i++)
            {
                foreach (string file in chapterFiles[i])
                {
                    // First array is base template file, load it as such
                    // If template load fails, then abort
                    if (i == 0)
                    {
                        string p = file != _srcPath ? Path.Join(_chpPath, file) : file;
                        try
                        {
                            if (File.Exists(p))
                            {
                                LoadTemplateFile(p);
                                break;
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO: expose exception message
                            //return false;
                            throw new ChapterEngineException($"CHAPTER EXCEPTION: Could not apply Chapter Template file: {p}!", e);
                        }
                    }
                    // all others are not base and are loaded as an override
                    else
                    {
                        string p = Path.Join(_chpPath, file);
                        try
                        {
                            if (File.Exists(p))
                            {
                                LoadOverrideCSV(IO.ParseStringCSV(p, false, false, true, true));
                                break;
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO: expose exception message
                            // Skip this override
                            throw new ChapterEngineException($"CHAPTER EXCEPTION: Could not apply Chapter Override file: {p}!", e);
                        }
                    }
                }
            }

            if (Editions[0].Chapters.Count > 0)
            {
                UpdateTimes();
                FillEmptyUIDs();
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool LoadTemplateFile(string path)
        {
            switch (Path.GetExtension(path))
            {
                case ".csv":
                    LoadTemplateCSV(IO.ParseStringCSV(path, false, false, true, true));
                    return true;
                
                case ".xml":
                    return LoadTemplateXML(File.ReadAllText(path));

                case ".mkv":
                case ".mka":
                case ".mks":
                case ".temp":
                    return LoadTemplateXML(Mkvmerge.RunMtnApp("mkvextract", $"chapters \"{_srcPath}\""));

                default:
                    throw new ChapterEngineException($"EXCEPTION: {Path.GetExtension(path)} files are not supported.", null);
            }
        }

        private void LoadTemplateCSV(string[][] data)
        {
            Editions.Add(new Edition());

            foreach(string[] cd in data)
            {   
                if (cd.Length < 1)
                    Editions[0].Chapters.Add(new ChapterAtom());
                else
                {
                    string starttime = cd.Length > 1 ? cd[1] : "";
                    string endtime = cd.Length > 2 ? cd[2] : "";

                    List<ChapterDisplay> displays = new List<ChapterDisplay>();
                    if (cd.Length > 3)
                    {
                        for (int i = 3; i < cd.Length; i = i+2)
                            displays.Add(new ChapterDisplay(i+1 < cd.Length ? cd[i+1] : "", cd[i]));
                    }
                    else
                        displays.Add(new ChapterDisplay());

                    Editions[0].Chapters.Add(new ChapterAtom(starttime, endtime, displays));
                }       
            }
        }

        private bool LoadTemplateXML(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                XDocument xml = XDocument.Parse(data);
                IEnumerable<XElement> editions = xml.Element("Chapters").Elements("EditionEntry");
                foreach (XElement ed in editions)
                {
                    List<ChapterAtom> cas = new List<ChapterAtom>();
                    IEnumerable<XElement> chapterAtoms = ed.Elements("ChapterAtom");
                    foreach (XElement ca in chapterAtoms)
                    {
                        List<ChapterDisplay> cds = new List<ChapterDisplay>();
                        IEnumerable<XElement> chapterDisplays = ca.Elements("ChapterDisplay");
                        foreach (XElement cd in chapterDisplays)
                        {
                            XElement Name = cd.Element("ChapterString");
                            XElement Lang = cd.Element("ChapterLanguage");
                            XElement LangIETF = cd.Element("ChapLanguageIETF");
                            XElement Country = cd.Element("ChapterCountry");
                            cds.Add(new ChapterDisplay(
                                    Name != null ? Name.Value : null,
                                    Lang != null ? Lang.Value : null,
                                    LangIETF != null ? LangIETF.Value : null,
                                    Country != null ? Country.Value : null
                                    ));
                        }

                        XElement ChapterUID = ca.Element("ChapterUID");
                        XElement ChapterFlagHidden = ca.Element("ChapterFlagHidden");
                        XElement ChapterFlagEnabled = ca.Element("ChapterFlagEnabled");
                        XElement ChapterTimeStart = ca.Element("ChapterTimeStart");
                        XElement ChapterTimeEnd = ca.Element("ChapterTimeEnd");
                        cas.Add(new ChapterAtom(
                                ChapterUID != null ? ChapterUID.Value : null, 
                                ChapterFlagHidden != null ? ChapterFlagHidden.Value : null, 
                                ChapterFlagEnabled != null ? ChapterFlagEnabled.Value : null,
                                ChapterTimeStart != null ? ChapterTimeStart.Value : null,
                                ChapterTimeEnd != null ? ChapterTimeEnd.Value : null,
                                cds
                                ));
                    }

                    XElement EditionUID = ed.Element("EditionUID");
                    XElement EditionFlagHidden = ed.Element("EditionFlagHidden");
                    XElement EditionFlagDefault = ed.Element("EditionFlagDefault");
                    
                    Editions.Add(new Edition(
                                EditionUID != null ? EditionUID.Value : null,
                                EditionFlagHidden != null ? EditionFlagHidden.Value : null,
                                EditionFlagDefault != null ? EditionFlagDefault.Value : null,
                                cas
                                ));
                }
                return true;
            }
            else
            {
                Editions.Add(new Edition());
                return false;
            }
        }

        private void LoadOverrideCSV(string[][] data)
        {
            List<string[]> chapterData = data.ToList();
            if (Editions.Count > 0)
                for (int i = 0; i < chapterData.Count; i++)
                {
                    string[] cd = chapterData[i];
                    if (cd != null && cd.Length > 0)
                    {
                        string opts = cd[0];
                        if (opts.Contains('-'))
                        {
                            Editions[0].Chapters.RemoveAt(i);
                            chapterData.RemoveAt(i);
                            i--;
                            continue;
                        }
                        
                        string starttime = cd.Length > 1 ? cd[1] : "";
                        string endtime = cd.Length > 2 ? cd[2] : "";

                        List<ChapterDisplay> displays = new List<ChapterDisplay>();
                        if (cd.Length > 3)
                        {
                            for (int j = 3; j < cd.Length; j = j+2)
                                displays.Add(new ChapterDisplay(j+1 < cd.Length ? cd[j+1] : "", cd[j]));
                        }

                        if (opts.Contains('+'))
                        {
                            ChapterAtom ca = new ChapterAtom(starttime, endtime, displays);
                            Editions[0].Chapters.Insert(i, ca);
                        }
                        else if (i < Editions[0].Chapters.Count)
                        {
                            if (starttime == "-")
                                Editions[0].Chapters[i].StartTime  = null;
                            else if (!string.IsNullOrEmpty(starttime))
                                Editions[0].Chapters[i].StartTime  = new ChapterTime(starttime);
                            
                            if (endtime == "-")
                                Editions[0].Chapters[i].EndTime = null;
                            else if (!string.IsNullOrEmpty(endtime))
                                Editions[0].Chapters[i].EndTime = new ChapterTime(endtime);

                            for (int j = 0; j < displays.Count; j++)
                            {
                                ChapterDisplay cdOrig = j < Editions[0].Chapters[i].Displays.Count ? Editions[0].Chapters[i].Displays[j] : null;
                                ChapterDisplay cdOvrd = displays[j];

                                if (cdOrig == null)
                                    Editions[0].Chapters[i].Displays.Add(cdOvrd);
                                else
                                {
                                    if (!string.IsNullOrEmpty(cdOvrd.Name))
                                        cdOrig.Name = cdOvrd.Name;
                                    if (!string.IsNullOrEmpty(cdOvrd.Language))
                                        cdOrig.Language = cdOvrd.Language;
                                    if (!string.IsNullOrEmpty(cdOvrd.LanguageIETF))
                                        cdOrig.LanguageIETF = cdOvrd.LanguageIETF;
                                    if (!string.IsNullOrEmpty(cdOvrd.Country))
                                        cdOrig.Country = cdOvrd.Country;
                                }
                            }
                        }
                        else    // no more chapters left for remaining override entries, ignore the rest of the override entries
                            return;
                    }       
                }
            else
                throw new ChapterEngineException("Cannot load chapter override data when there is no existing base chapter data loaded.", null);
        }
    
        private void UpdateTimes()
        {
            if (Editions[0].Chapters.Count > 0)
            {
                for (int i = 0; i < Editions[0].Chapters.Count - 1; i++)
                if (Editions[0].Chapters[i].EndTime == null && Editions[0].Chapters[i+1].StartTime != null)
                    Editions[0].Chapters[i].EndTime = new ChapterTime(Editions[0].Chapters[i+1].StartTime.Time);

                for (int i = Editions[0].Chapters.Count - 1; i > 0; i--)
                    if (Editions[0].Chapters[i].StartTime == null && Editions[0].Chapters[i-1].EndTime != null)
                        Editions[0].Chapters[i].StartTime = new ChapterTime(Editions[0].Chapters[i-1].EndTime.Time);

                // If 1st Start Timestamp is still null, set to 0
                if (Editions[0].Chapters[0].StartTime == null)
                    Editions[0].Chapters[0].StartTime = new ChapterTime("00:00:00.000000000");

                // If Last Timestamp is still null, use mkvinfo to get file duration from provided source file
                if (Regex.IsMatch(Path.GetExtension(_srcPath), ".mk.") || Path.GetExtension(_srcPath) == ".temp")
                {
                    string duration = Regex.Match(Mkvmerge.RunMtnApp("mkvinfo", "\"" + _srcPath + "\""), "Duration: ..:..:............").Value.Substring(10);
                    if (Editions[0].Chapters[Editions[0].Chapters.Count - 1].EndTime == null)
                        Editions[0].Chapters[Editions[0].Chapters.Count - 1].EndTime = new ChapterTime(duration);
                }
            }
        }

        private void FillEmptyUIDs()
        {
            for (int i = 0; i < Editions.Count; i++)
            {
                if (Editions[i].UID == 0)
                    Editions[i].UID = GetRandomUID();

                for (int j = 0; j < Editions[i].Chapters.Count; j++)
                    if (Editions[i].Chapters[j].UID == 0)
                        Editions[i].Chapters[j].UID = GetRandomUID();
            }
        }

        public string ExportToCSV()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < Editions[0].Chapters.Count; i++)
            {
                ChapterAtom c = Editions[0].Chapters[i];
                sb.Append($"\t{c.StartTime}\t{c.EndTime}");
                foreach (ChapterDisplay d in c.Displays)
                    sb.Append($"\t{d.Language}:{d.LanguageIETF}:{d.Country}\t{d.Name}");
                
                if (i < Editions[0].Chapters.Count - 1)
                    sb.AppendLine();
            }

            return sb.ToString();
        }

        public string ExportToXML()
        {
            XDocument result = new XDocument();
            result.Declaration = new XDeclaration("1.0", null,null);
            result.Add(new XComment(" <!DOCTYPE Chapters SYSTEM \"matroskachapters.dtd\"> "));
            result.Add(new XElement("Chapters"));

            XElement edition = new XElement("EditionEntry");
            edition.Add(new XElement("EditionFlagHidden", Editions[0].Hidden ? "1" : "0"));
            edition.Add(new XElement("EditionFlagDefault", Editions[0].Default ? "1" : "0"));
            edition.Add(new XElement("EditionUID", Editions[0].UID));

            foreach (ChapterAtom ca in Editions[0].Chapters)
            {
                XElement atom = new XElement("ChapterAtom");
                atom.Add(new XElement("ChapterUID", ca.UID));
                atom.Add(new XElement("ChapterTimeStart", ca.StartTime.Time));
                atom.Add(new XElement("ChapterFlagHidden", ca.Hidden ? "1" : "0"));
                atom.Add(new XElement("ChapterFlagEnabled", ca.Enabled ? "1" : "0"));
                atom.Add(new XElement("ChapterTimeEnd", ca.EndTime.Time));

                foreach (ChapterDisplay cd in ca.Displays)
                {
                    XElement display = new XElement("ChapterDisplay");
                    if (cd.Name != null) display.Add(new XElement("ChapterString", cd.Name));
                    if (cd.Language != null) display.Add(new XElement("ChapterLanguage", cd.Language));
                    if (cd.LanguageIETF != null) display.Add(new XElement("ChapLanguageIETF", cd.LanguageIETF));
                    if (cd.Country != null) display.Add(new XElement("ChapterCountry", cd.Country));

                    atom.Add(display);
                }

                edition.Add(atom);
            }

            result.Root.Add(edition);
            return result.Declaration.ToString() + Environment.NewLine + result.ToString();
        }

        private ulong GetRandomUID()
        {
            byte[] buf = new byte[8];
            r.NextBytes(buf);
            return BitConverter.ToUInt64(buf);
        }
    }
}
