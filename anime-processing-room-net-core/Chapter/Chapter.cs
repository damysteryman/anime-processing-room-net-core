using System;
using System.Collections.Generic;

namespace anime_processing_room_net_core
{
    public class ChapterDisplay
    {
        private string _lang = "unk";
        public string Name { get; set; }
        public string Language
        { 
            get
            {
                return _lang;
            }
            set
            {
                _lang = value == null ? null : value.Substring(0, 3);
            }
        }
        public string LanguageIETF { get; set; }
        public string Country { get; set; }

        public ChapterDisplay()
        {
            Name = "";
        }
        public ChapterDisplay(string name, string lang, string langIETF, string country)
        {
            Name = name;
            Language = lang;
            LanguageIETF = langIETF;
            Country = country;
        }

        public ChapterDisplay(string name, string localeInfo)
        {
            Name = name;

            string[] data = localeInfo.Split(":");
            if (!string.IsNullOrEmpty(data[0]))
                Language = data[0];
            if (data.Length > 1 && !string.IsNullOrEmpty(data[1]))
                LanguageIETF = data[1];
            if (data.Length > 2 && !string.IsNullOrEmpty(data[2]))
                Country = data[2];
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class ChapterAtom
    {
        public ulong UID { get; set; }
        public ChapterTime StartTime { get; set; }
        public ChapterTime EndTime { get; set; }
        public bool Hidden { get; set; } = false;
        public bool Enabled { get; set; } = true;
        public List<ChapterDisplay> Displays { get; set; } = new List<ChapterDisplay>();

        public ChapterAtom()
        {
            UID = 0;
            Displays.Add(new ChapterDisplay());
        }

        public ChapterAtom(string starttime, string endtime, List<ChapterDisplay> chapterdisplays)
        {
            StartTime = !string.IsNullOrEmpty(starttime) ? new ChapterTime(starttime) : null;
            EndTime = !string.IsNullOrEmpty(endtime) ? new ChapterTime(endtime) : null;
            Displays = chapterdisplays;
        }

        public ChapterAtom(string uid, string hidden, string enabled, string starttime, string endtime, List<ChapterDisplay> chapterdisplays)
            : this(starttime, endtime, chapterdisplays)
        {
            UID = ulong.Parse(uid);
            if (hidden != null)
                Hidden = hidden == "1";
            if (enabled != null)
                Enabled = enabled == "1";
        }

        public override string ToString()
        {
            return $"{(StartTime != null ? StartTime.Time : "null")} - {(EndTime != null ? EndTime.Time : "null")}";
        }
    }

    public class Edition
    {
        public ulong UID { get; set; }
        public bool Hidden { get; set; } = false;
        public bool Default { get; set; } = true;
        public List<ChapterAtom> Chapters { get; set; }

        public Edition()
        {
            Chapters = new List<ChapterAtom>();
        }

        public Edition(string uid, string hidden, string def, List<ChapterAtom> chapters)
        {
            UID = ulong.Parse(uid);
            if (hidden != null )
                Hidden = hidden == "1";
            if (def != null)
                Default = def == "1";
            Chapters = chapters;
        }
    }
    public class ChapterTime
    {
        private DateTime _wholeTime { get; set; }
        private uint _nanoseconds { get; set; }

        public string Time
        {
            get
            {
                return $"{_wholeTime.ToString("HH:mm:ss")}.{_nanoseconds.ToString("000000000")}";
            }

            set
            {
                string[] bits = value.Split('.');
                
                
                _wholeTime = DateTime.Parse(bits[0]);

                if (bits.Length > 1)
                {
                    string tmp = bits[1].TrimEnd('0');
                    uint val = !string.IsNullOrEmpty(tmp) ? uint.Parse(tmp) : 0;
                    int power = 9 - tmp.Length;
                    
                    uint multiplier = (uint)(Math.Pow(10, power));
                    _nanoseconds = val * multiplier;
                }
                
            }
        }

        public ChapterTime(string time)
        {
            Time = time.Replace(',', '.');
        }

        public override string ToString()
        {
            return Time;
        }
    }
}