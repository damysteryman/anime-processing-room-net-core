# CHANGELOG
---
## v1.17.2

### Track Description Fix

AnimeProcessingRoom/Source.cs:

- BUGFIX: Allowed / and \ to be used inside of track csv files (e.g. in Track Descriptions)

Directory.Build.props:

- UPDATE: Updated Copyright Year

---
## v1.17.1

### TrackTable Display Fix

UI/TrackTable.cs:

- BUGFIX: Fixed Final Sorted Track Table ID Column default length being too short

---
## v1.17.0

### ChapterEngine Clear Start/End Times

Chapter/ChapterEngine.cs:

- NEW: Added ability to clear out a Chapter Start Time or End Time by entering "-" in the Time fields

---
## v1.16.1

### Chapter Sync Bugfix

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: Fixed Chapter Sync value being ignored by mkvtoolnix

---
## v1.16.0

### Chapter Source Index Selection + Bugfixes

anime-processing-room-net-core.csproj:
- UPDATE: Updated .NET to 8.0
- UPDATE: Updated Copyright Year 

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- NEW: Added ability to use a specified Source Index to use as a base for Chapter Engine, instead of always defaulting to first found Source

AnimeProcessingRoom/AnimeProcessingRoomArgs.cs:

- NEW: Added ChapterSourceIdx Settings to hold user-specified Chapter Source Index

Chapter/Chapter.cs:

- BUGFIX: Fixed ChapterAtom & Chapter Edition Default/Hidden/Enabled flags not being set correctly by default

Docs/Sources.md:

- NEW: Added Sources.md basic documentation

Program.cs:

- NEW: Added "--chapter-source-index" parameter

UI/UI.cs:

- NEW: Display Chapter Source Index & Chapter Sync (in ms) when using user specified Chapter Source Index
- NEW: Added "--chapter-source-index" to help text
- BUGFIX: Fix incorrect "--dest-filename-format" help text & tidy up rest of text a bit

---
## v1.15.9

### Chapter Language Bugfix

Chapter/Chapter.cs:

- BUGFIX: Added missing null check to Chapter Language field

---
## v1.15.8

### Even More ChapterEngine Bugfixes

- BUGFIX: ChapterEngine attempted to export chapter XML even when there were no chapters to begin with

---
## v1.15.7

### More ChapterEngine Bugfix

- BUGFIX: ChapterEngine attempted to try to auto-update Chapter Times even when there were no chapters to begin with

---
## v1.15.6

### New Chapter Adding Feature Bugfix

Chapter/ChapterEngine.cs:

- BUGFIX: New chapters could not be added using ChapterEngine CSV format if the source file didn't already have chapter data to begin with

---
## v1.15.5

### Bugfix for Skip feature

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: Episodes marked to be skipped were still being processed even though they were not going to be built

---
## v1.15.4

### Fixed dynamic data application from dyn-track-titles.csv to both Track Name and Track Source strings

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: DynTrackTitles values were null in some cases

UI/TrackTable.cs:

- BUGFIX: TrackTables now properly show Dynamic Titles for both Track names and Track Sources

---
## v1.15.3

### Track Bugfix

AnimeProcessingRoom/Track.cs:

- BUGFIX: Track Flags would sometimes not be initialized correctly.

---
## v1.15.2

### More bugfixes

AnimeProcessingRoom/AnimeProcessingRoom.cs

- BUGFIX: App would try to apply Dynamic Track Titles even if the weren't any, causing the app to fail in the process.
- BUGFIX: In some cases, app would attempt to process Source data even if there wasn't any.

---
## v1.15.1

### Source file prep bugfix

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: App would not attempt to prep non-Matroska-based Source files if there was only 1 of them

---
## v1.15.0

### Added support for new MKV Flags supported by MKVToolNix

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- NEW: Added support for new MKV Flags supported by MKVToolNix

AnimeProcessingRoom/Track.cs:

- NEW: Added support for new MKV Flags supported by MKVToolNix

UI/TrackTable.cs:

- NEW: Added support for new MKV Flags supported by MKVToolNix

---
## v1.14.3

### UI Source and Track Table Bugfixes

UI/UI.cs:

- BUGFIX: Source Table generator still added Sources to current Episode's table even if they were not used by that Episode
- BUGFIX: Track Table generator did not update Track IDs when ignoring sources unused by current Episode, causing some IDs to go out of sync

Program.cs:
- UPDATE: App now uses updated Source Table and Track table generators for display output

---
## v1.14.2

### Bugfixes + Chapter Engine ExportToCSV() added 

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- NEW: Added ability to store and expose data for which Sources are actually available/enabled for each Episode
- BUGFIX: Source Processing would attempt to create/add tracks for null/blank Source entries in Source CSV file

Chapter/ChapterEngine.cs:

- NEW: Added ExportToCSV() method to export current ChapterEngine data to a Template CSV file
- UPDATE: Failure to apply Chapter initial Template file now throws an exception instead of being ignored
- BUGFIX: UpdateTimes() attempted to get video end time from non-video source files (such as .xml)
- BUGFIX: Application would fail upon attempting to load Chapter data from a Matroska file if the file path contained spaces

Program.cs:

- UPDATE: TrackTable creation now creates tables for an Episode using only the Sources that are actually available/enabled for that Episode

---
## v1.14.1

### Fixed FINAL TRACK TABLE still not displaying Dynamic Track Titles correctly

AnimeProcessingRoom/Track.cs:

- NEW: Added new Track Constructor

Program.cs:

- BUGFIX: FINAL TRACK TABLE was still not displaying Dynamic Track Titles correctly

UI/TrackList.cs:

- NEW: Added method to add new modifiable Track List to TrackTable for display reasons instead of referencing original List

UI/UI.cs:

- BUGFIX: FINAL TRACK TABLE was still not displaying Dynamic Track Titles correctly

---
## v1.14.0

### Changed default base paths for referencing Source content and Metadata instead of using Current Working Dir, Updated UI display of Tracks with Dynamic Titles, Bugfixes

AnimeProcessingRoom/AnimeProcessingRoom.cs

- UPDATE: App now uses "src" subdir as base path for source content dirs, and "meta/{season}" subdir as basepath for metadata content dirs as default, instead of "{Working Dir}" and "{Working Dir}/{season}" respectively
- BUGFIX: App would fail upon trying to get source filenames from a source directory that does not exist or is inaccesible.

AnimeProcessingRoom/AnimeProcessingRoomArgs.cs

- NEW: New parameter added ("--source-base-dir") to allow for overriding base subdir for holding Source content
- UPDATE: App now uses "src" subdir as base path for source content dirs, and "meta/{season}" subdir as basepath for metadata content dirs as default, instead of "{Working Dir}" and "{Working Dir}/{season}" respectively

AnimeProcessingRoom/IO.cs

- BUGFIX: Last NewLine/Entry in Metadata CSV files were being ignored when empty

AnimeProcessingRoom/Source.cs:

- UPDATE: App now uses "src" subdir as base path for source content dirs, and "meta/{season}" subdir as basepath for metadata content dirs as default, instead of "{Working Dir}" and "{Working Dir}/{season}" respectively

Chapter/Chapter.cs:

- BUGFIX: Decimal points for Chapter Times were not being sanitized properly (from comma to period)

Chapter/ChapterEngine.cs:

- BUGFIX: ChapterEngine would fail if Chapter Override metadata had more override entries than there were actual Chapter data
- BUGFIX: Fixed typos

Program.cs:

- NEW: Added ability to override base subdir for holding Source content
- UPDATE: App now displays Track names in output with Dynamic Track Titles applied to them

UI/TrackTable.cs:

- UPDATE: App now displays Track names in output with Dynamic Track Titles applied to them

UI/UI.cs:

- UPDATE: App now displays Track names in output with Dynamic Track Titles applied to them
- UPDATE: Usage updated to include new "--source-base-dir" parameter

---
## v1.13.1

### Internal CSV Parsing Changes + Some Bugfixes

AnimeProcessingRoom/IO.cs:

- UPDATE: Set default minColumns for IO.ParseStringCSV to 0
- UPDATE: IO.ParseStringCSV now sets line array to null when it has 0 columns, and creates empty array when minColumns is larger than 0

AnimeProcessingRoom/Source.cs:

- BUGFIX: Source.GetTrackListMeta() was creating empty Tracks out of Tracks set to be ignored, instead of actually ignoring them with a null Track

UI/UI.cs:

- BUGFIX: Fixed typo in UI.Usage(), "--chapters-dir" was misspelled as "--chapter-dir"

---
## v1.13.0

### Multiple Default Template/Override CSV Support in ChapterEngine

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- NEW: Added custom Exception type
- NEW: Added ability to specify multiple ChapterEngine default template+override indexes on a per-episode basis using "ce-defaults-table.csv" metadata file
- NEW: Added support to override path to "ce-defaults-table.csv"
- UPDATE: Made sure all areas that need CSV data parsing use the IO.ParseStringCSV() or IO.ParseIntCSV() methods
- UPDATE: Updated method permissions to better reflect actual use cases
- UPDATE: Started adding more exception handling
- UPDATE: Moved Source CSV metadata file parsing from Source constructor to AnimeProcessingRoom core
- UPDATE: Other minor cleanup

AnimeProcessingRoom/IO.cs:

- NEW: Added custom Exception type
- NEW: Added support for ignoring comments (lines starting with '#' or '//') inside most CSV metadata fiels (except dyn-track-titles.csv)
- NEW: Added boolean option to IO.ParseStringCSV() to decide to either return a default placeholder array, or throw an exception, upon CSV file parse failure
- UPDATE: Rewrote IO.ParseStringCSV() adding various settings needed by all CSV metadata file variations
- UPDATE: Added cross-platform directory separator auto-conversion into IO.ParseStringCSV() for loading Source CSV metadata files
- BUGFIX: Allow Jagged 2D Array output check was the wrong way around, causing jagged array when false, and rectangular array when true
- BUGFIX: Generated output array could have had less columns than the specified minimum if the source data's column length was less than the minimum

AnimeProcessingRoom/Source.cs:

- NEW: Added custom Exception type
- NEW: Added ability to properly throw exceptions for some common failure scenarios
- UPDATE: Moved Source CSV metadata file parsing from Source constructor to AnimeProcessingRoom core
- UPDATE: Moved Track CSV metadata file parsing from Track constructor to Source.GetTrackListMeta() method

AnimeProcessingRoom/Track.cs:

- UPDATE: Moved Track CSV metadata file parsing from Track constructor to Source.GetTrackListMeta() method
- UPDATE: Minor cleanup

Chapter/ChapterEngine.cs:

- NEW: Added ability to use multiple default template/override CSV files using filenames "defaultN-template.csv" and "defaultN-override.csv", where N is a number corresponding to which index file to use. The 2 index numbers are passed as args to ChapterEngine.Process() with the default usage being ChapterEngine.Process(0, 0).
- UPDATE: Made sure all areas that need CSV data parsing use the IO.ParseStringCSV() method
- UPDATE: Updated method permissions to better reflect actual use cases

Docs/Chapters.md:

- NEW: Added section about using multiple default template/override CSV files and specifying them with ce-defaults-table.csv
- UPDATE: Updated Chapter Engine Data File Loading Order/Priority table with dynamic default template/override filenames

Program.cs:

- NEW: Added support to override path to "ce-defaults-table.csv", use "--cedt-file" parameter to do it

UI/UI.cs:

- UPDATE: Chapter Table now displays the selected Default Template Idx + Default Override Idx passed to ChapterEngine for the current displayed Episode
- UPDATE: Updated UI.Usage() to display how to use "--cedt-file" to override path to "ce-defaults-table.csv"

---
## v1.12.3

### IO CSV Parsing Bugfix

AnimeProcessingRoom/IO.cs:

- BUGFIX: App would fail upon attemptiong to load a Metadata CSV file with a row count lesser than an an Episode No. of the episodes being processed

---
## v1.12.2

### UI & Chapter Engine Bugfixes

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: Episode Offset was not actually being used where it should have been

Chapter/ChapterEngine.cs:

- BUGFIX: 1st Chapter Start Timestamp or Last Chapter End Timestamp not being set made Chapter Engine fail on Chapter Data Export

---
## v1.12.1

### Fixed --help option not actually printing help usage

Program.cs:

- BUGFIX: --help option stopped printing help/usage message

---
## v1.12.0

### Added support for calculating CRC32 of output file and adding it to filename, Chapters documentation update, Core + UI code Refactor/Cleanup

anime-processing-room-net-core.csproj:

- NEW: Added PackageReference to [Crc32.NET package](https://www.nuget.org/packages/Crc32.NET/) for CRC32 related functionality

AnimeprocessingRoom/AnimeProcessingRoom.cs:

- NEW: Added ability to calculate CRC32 of output file and add it to its file name, using {c} or {crc32} item in Destination Filename Format string
- UPDATE: Updated Format string to accept new item keywords: name, season, episode, title, c, crc32, such that both single-letter and full-word versions are supported
- UPDATE: Updated override file check to take into account CRC32 field in filename as a wildcard
- UPDATE: Moved the AnimeProcessingRoomArgs class to its own file
- UPDATE: Moved the MsgArgs class to its own file
- UPDATE: TIdied up the file with regions and rearranging the positions of some methods in the file

AnimeProcessingRoom/AnimeProcessingRoomArgs.cs:

- NEW: Moved the AnimeProcessingRoomArgs class to its own file

Docs/Chapters.md:

- NEW: Added minor info regarding locale setting
- NEW: Added example chapter row to CSV Format Table
- BUGFIX: Fixed Chapter Engine CSV Format Structure Table

Docs/EpisodeTitleFormat.md:

- NEW: Added new item keywords and CRC32 related options to Format string doc

.gitlab-ci.yml:

- BUGFIX: Added missing .zip extension to release file names in release post

Misc/MsgArgs.cs:

- NEW: Moved the MsgArgs class to its own file

Misc/Utils.cs:

- NEW: Added method to calculate CRC32 of a file

Program.cs:

- UPDATE: Minor section subtitle update (Build section now displays both mkvmerge output and CRC32 related output)
- UPDATE: Updated app to implement refactor changes made to UI/UI.cs
- UPDATE: Minor commenting/cleanup

UI/StringList.cs:

- UPDATE: StringList class made obsolete, replaced with List<string>
- UPDATE: Obsolete UI/StringList.cs file deleted

UI/TrackTable.cs:

- NEW: Moved TrackTable class into its own file

UI/UI.cs:

- UPDATE: Moved StringList.PutInBorder() method into UI.cs as List<string>.PutInBorder(), making StringList class obsolete
- UPDATE: Replaced references to obsolete StringList class with List<string>
- UPDATE: Moved TrackTable class into its own file

---
## v1.11.0

### Replaced various episode title + filename settings with a dynamic string format specification ability + .NET 5.0 support

anime-processing-room-net-core.csproj:

- UPDATE: Changed version of .NET used from .NET Core 3.1 to .NET 5.0

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- NEW: Made new custom string format for dynamically creating Episode Titles, Source File Prefixes, and Destination Filenames
- UPDATE: Removed old options that have been made obsolete from the above mentioned new feature
- UPDATE: Some cleanup
- BUGFIX: App tried to process an episode even when no source files were available for it

Directory.Build.props:

- NEW: Added Copyright, Title, and Product fields

Docs/Chapters.md:

- UPDATE: Rewrote Chapters documentation breifly outlining how to use both Traditional XML and Chapter Engine to process Chapters for output MKVs

Docs/EpisodeTitleFormat.md

- NEW: Added Doc file outlining Custom Title Format specification for dynamic string format specification ability.

.gitlab-ci.yml:

- NEW: Added docs into release by zipping together binary + docs files
- UPDATE: Cleanup; Consolidated setup and build stages
- UPDATE: Changed version of .NET used from .NET Core 3.1 to .NET 5.0

Program.cs:

- UPDATE: Replaced old terminal args with new dynamic string format options
- UPDATE: Minor cleanup
- BUGFIX: App tried to process an episode even when no source files were available for it

publish_scripts/release-publish-local.sh

- UPDATE: Ported .NET 5.0 support from .gitlab-ci.yml to release-publish-local.sh

publish_scripts/release-template.md

- UPDATE: Reference .NET 5.0 instead of .NET Core 3.1

UI/StringList.cs:

- BUGFIX: PutInBorder() was trying to add a border to a StringList even if the StringList was empty

UI/UI.cs:

- UPDATE: Updated Usage() to remove old args and add new ones
- BUGFIX: ChapterTable() was trying to generate a table from Chapter data even when no Chapter data was available

.vscode/launch.json:

- UPDATE: Updated launch.json to use args for the new options instead of old removed ones

---
## v1.10.3

### Yet another bugfix

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: Episode Title was not being correctly created when Episode Title Type was set to Type #2 (EpTitleType.NAME_TITLE_EPPREFIX_ONLY) 

Release-publish.sh:

- UPDATE: Push to v1.10.3

---
## v1.10.2

### Another Bugfix

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- BUGFIX: App attempted to process Track Orders regardless of whether or not they were even supplied by the user

---
## v1.10.1

### Bugfixes

Program.cs:

- BUGFIX: App was not exiting when AnimeProcessingRoom initial LoadData() failed

Release-publish.sh:

- UPDATE: Push to v1.10.1

UI/UI.cs:

- BUGFIX: GetFinalTrackTable() was not ignoring and omitting the IGNORED Tracks, causing it to fail

---
## v1.10.0

### Major internal refactor, separated Frontend/UI code and Core code

AnimeProcessingRoom/AnimeProcessingRoom.cs:

- NEW: Added missing Skip Episodes option to Settings
- NEW: Added missing Episode Offset option to Settings
- NEW: Moved core logic out of Program.cs and into AnimeProcessingRoom.cs
- UPDATE: Removed all UI-related code to UI/UI.cs and Program.cs
- UPDATE: Text output generated by AnimeProcessing Room core is now given to UI using Events instead of using Console.WriteLine()

AnimeProcessingRoom/Source.cs:

- UPDATE: Moved GetAllSources.cs over to AnimeProcessingRoom.cs
- UPDATE: Replaced all instances of obsolete TrackList with List<Track>
- UPDATE: Moved GetAllTracks() from Track.cs to Source.cs as GetAllTrackMetadata()

AnimeProcessingRoom/Track.cs:

- UPDATE: Moved GetAllTracks() from Track.cs to Source.cs as GetAllTrackMetadata()

AnimeProcessingRoom/TrackList.cs:

- UPDATE: SetTrackTypes() was moved to AnimeProcessingRoom core
- UPDATE: TrackList class was made obsolete, replaced with List<Track>, TrackList.cs deleted

Chapter/ChapterEngine.cs:

- BUGFIX: Trying to apply Chapter Overrides over Chapter Data with different amounts of ChapterDisplays would cause ChapterEngine to fail

Misc/MkvMerge.cs:

- NEW: Added MkvMergeProcess class to run MkvMerge app with supplied args, but using an Event to provide output text to subscribed objects in real-time instead of relying on Console.WriteLine()
- UPDATE: Removed old RunMkvMerge() method

Program.cs:

- UPDATE: Moved core logic out of Program.cs and into AnimeProcessingRoom.cs
- NEW: Program now uses AnimeProcessingRoom core object to do core work, using Event subscription to handle any text output
- NEW: Added Episode Chapter Table display to app
- NEW: Added Final Output Track table to show the final in-order list of tracks to be in an episode's output mkv file
- UPDATE: UI stuff now uses methods from UI to display all human-readable data tables
- UPDATE: MkvToolNix tools check now moved from Program.cs to AnimeProcessingRoom.cs

Release-publish.sh:

- UPDATE: Changed git hash display from 7 to 8 digits
- UPDATE: Pushed to v1.10.0

UI.cs:

- NEW: Added ChapterTable method to create human-eadable display of generated chapters for an episode
- NEW: Added GetFinalSortedTable to TrackTable to create human-readable Track table, comprising only the tracks that will be present in the final episode mkv, sorted by TrackOrder
- UPDATE: Moved FileBanner and SourceTable generation methods from core to here
- UPDATE: TrackTable now uses Tracks included IDs instead of creating its own

---
## v1.9.1

Chapter/ChapterEngine.cs:

- BUGFIX: .temp file extension was missing from list of dile extensions recognized by ChapterEngine

---
## v1.9.0

### Added Chapter Engine to support naming/editing chapter support

Chapter/Chapters.cs:

- NEW: Added Chapter Engine to support naming/editing chapter support
- UPDATE: Completed initial version of Chapter Engine

Chapter/ChapterEngine.cs:

- NEW: Added Chapter Engine to support naming/editing chapter support
- UPDATE: Completed initial version of Chapter Engine
- BUGFIX: Chapter base template fles other than the source mkv were not being loaded
- BUGFIX: Malformed override csv files could cause the Chapter Engine to attempt to edit a chapter marked for deletion, causing the following chapter to be incorrectly edited instead

Docs/Chapters.md

- NEW: Added Docs folder and Chapters.md basic info file explaining which chapter metadata files are used by Chapter Engine
- NEW: Added --chapter-dir option to override default path from which to load chapter metadata files

MkvMerge.cs:

- UPDATE: Minor refactor/cleanup

Program.cs:

- NEW: Added support to use Chapter Engine to dynamically edit Chapters based on new metadata files
- UPDATE: Changed directory for raw chapter xml files
- BUGFIX: App was not checking for existence of necessary 'mkvextract' tool during startup
- BUGFIX: Chapter Data from Chapter Engine was being added into mkv as a second edition/set instead of actually editing/replacing existing chapter edition/set in mkv file
- UPDATE: Minor refactor/cleanup

README.md:

- NEW: Added chapter editing to list of app capabilities

Release-publish.sh:

- UPDATE: Push to v1.9.0

Track.cs:

- UPDATE: Track Language entry now supports free-form IETF string, not just 3-char language string; Deprecated manual track format specification in track.csv format is now fully removed

TrackList.cs:

- UPDATE: Minor refactor

UI.cs:

- UPDATE: Added --chapter-dir usage to Usage display

---
## v1.8.2

Release-publish.sh:

- UPDATE: Added git commit version hash (7-digit) to program version number when compiling with Release-publish.sh

---
## v1.8.1

Program.cs:

- BUGFIX: Fixed issue where episode fulltitle could get generated with leading space under certain circumstances

UI.cs:

- NEW: Added author information to program

Release-publish.sh:

- UPDATE: Push to v1.8.1

---
## v1.8.0

Program.cs:

- UPDATE: Enforced --dest, --ep-min, --ep-max args as required args
- UPDATE: Changed parameteres to all use lower case characters
- UPDATE: Changed meta file default filenames to not include season no.
- NEW: Added ability to supply override path to the meta directory containing all the metadata-related files
- NEW: Added ability to supply override paths to all metadata-related files
- NEW: Added ability to skip episodes during batch process

Release-publish.sh:

- UPDATE: Updated version to 1.8

UI.cs:

- NEW: Finally added help screen to display brief app usage and arguments

---
## v1.7.0

Program.cs:

- NEW: Added ability to specify titles file to use instead of titles-sX.txt
- NEW: Added ability to specify slice-times file to use instead of slice-times-sX.txt
- NEW: Added ability to specify file extension of output files
- UPDATE: Made Episode Titles (titles-sX.txt) optional
- UPDATE: Use chapters from the first actually found source rather than always source no.1

---
## v1.6.0

Utils.cs:

- NEW: Added Ordinal option to NumberWord converter
- BUGFIX: NumberWord converter was reading a number's digits in the wrong order, causing output to be flipped (Ex: "Thirty-One" instead of "Thirteen")

Program.cs:

- NEW: Added epOptNum option No. 4: Episode number in Ordinal-NumberWord format (Ex: "First" for 1, "Second" for 2, etc.)

---
## v1.5.2

Program.cs:

- NEW: Added new epType (EpType 4) to place epTypePrefix after Episode No. as a suffix instead

---
## v1.5.1

Program.cs:

- UPDATE: PrepTrackArgs() now does dyn-track-title search and replace to both Track Name and Source rather than just Name

---
## v1.5.0

Program.cs

- UPDATE: Added new epType for showing Episode No. without prefix

---
## v1.4.0

Program.cs

- NEW: Added ability to change output file prefix using --epFilePrefix option. use {s} to represent season no. and {e} to represent episode no. in prefix. "S{s}E{e} - " is still the default.
- BUGFIX: creation of .temp source files only applied metadata to 1st track rather than all
- BUGFIX: --no-chapters arg was accidentally forced when external chapter files were being used

Release-publish.sh

- UPDATE: update to ver 1.4

---
## v1.3.0

### Redesigned UI

Program.cs

- UPDATE: PrepTrackArgs can now force include all tracks regardless of type
- UPDATE: Attachment arg preparations is now a separate method
- UPDATE: Moved UI related code to separate file for UI code (UI.cs)
- UPDATE: upgraded .temp source file creation to include attachments and metadata for that source

TrackList.cs

- UPDATE: Moved UI TrackList Table generation code to UI.cs

Utils.cs

- UPDATE: Moved UI related code to UI.cs
- UPDATE: Moved StringList class to its own file (StringList.cs)
- UPDATE: Moved IO related code to new file (IO.cs)

---
## v1.2.0

Mkvmerge.cs:

- NEW: Added code to run both mkvmerge, and mkvinfo and return output
- UPDATE: Added support to supply which executable to check for instead of just mkvmerge

Program.cs:

- UPDATE: Updated allowed file extensions to only allow Matroska container files for source files, other non-Matroska files will be wrapped into a temporary Matroska file
- NEW: Added ability to convert Episode Numbers to Roman numerals in Episode title
- NEW: Added ability to combine multiple source files into a single temporary source file, all files in a Source dir that start with SxxEyy will be combined into a .temp mkv file
- UPDATE: Track types (video/audio/subtitle) are now obtained from using output from the 'mkvinfo' app rather than from tracks.csv file, and Track type column in tracks.csv is now deprecated/unused. This column can still be present in the file but will be ignored.
- UPDATE: Minor cleanup

Track.cs

- UPDATE: Refactor/cleanup
- BUGFIX: TrackList creator failed to create final track from tracks.csv if the final track was marked as usuned, rather than creating blank Track
- BUGFIX: Track Source backwards traversal mechanism did not stop upon finding a non-blank Source value like it should have

TrackList.cs:

- NEW: Added ability to set All Track's Types using mkvinfo app

Utils.cs:

- NEW: Added Roman numeral method

Release-publish.sh:

- UPDATE: upfdate to version 1.2

---
## v1.1.2

Program.cs:

- UPDATE: --epOff argument is no longer compulsory
- UPDATE: Cleanup, classes were refactored into their separate files

Utils.cs:

- BUGFIX: The minimum no. of columns passed to ParseStringCSV was not being used

---
## v1.1.1

Program.cs:

- NEW: Added ability to specifiy no. of digits for Episode No. in file name
- BUGFIX: UI Ttrack Table column default lengths were incorrect
- BUGFIX: Sync times table min. columns value was not being set corectly, causing a crash when there were more sources than their were columns in sync times table

Release-build.sh:

- UPDATE: update to ver 1.1.1

---
## v1.1.0

Program.cs:

- NEW: Added ability to show Source and Track info cleanly during processing
- UPDATE: Updated rest of UI output to match style used by Source and Track display

Utils.cs:

- NEW: Added StringList-related code

Release-publish.sh:

- UPDATE: update to version 1.1
- BUGFIX: regex used to overwrite release version only worked for single-digit numbers

---
## v1.0.3

Program.cs:

- BUGFIX: Double-quotes in episode title were not being escaped properly

Utils.cs:

- BUGFIX: NumberWord function did not actually work to begin with

Release-publish.sh:

- UPDATE: update version to 1.0.3

CHANGELOG.md

- NEW: Added CHANGELOG file

---
## v1.0.2

Program.cs:

- BUGFIX: Only forward-slashes were usable in Track Meta FilePaths specified in sources-sX.csv, now both forward-and back-slashes are usable

Release-publish.sh:

- UPDATE: Push to version 1.0.2
- UPDATE: Changed publish settings to publish self-contained executable files for both portable and non-portable releases
- UPDATE: General code improvements in script (refactoring, more generic target list)

---
## v1.0.1

Program.cs:

- BUGFIX: Attachment, Track Order, and Slice Times arguments were being added even when the relevant data for them was not available.
- UPDATE: Added shortened -n and -d options for --name and --dest options respectively

Release-publish.sh:

- NEW: Added Release Publisher script (Release-publish.sh) with Version setting based on git repo revision