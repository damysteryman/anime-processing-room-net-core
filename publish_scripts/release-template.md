# DOWNLOAD {CI_COMMIT_TAG}

*Regular versions require .NET 8.0 to be installed!  
Portable versions have it self-contained, but are larger downloads as a result.*

Which file to download:

| Computer's OS Type | Already got .NET 8.0+ installed? | Choose this file on the Package Registry Page: |
| ------ | ------ | ------ |
| Windows | Yes | {WINDOZE_BINARY} |
| Windows | No | {WINDOZE_BINARY_PORTABLE} |
| Linux | Yes | {LINUX_BINARY} |
| Linux | No | {LINUX_BINARY_PORTABLE} |

## [Download {CI_COMMIT_TAG} here! (via GitLab Package Registry)]({DOWNLOAD_PAGE})

# CHANGELOG