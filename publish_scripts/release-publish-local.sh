#!/bin/bash

targets=("linux-x64:" "win-x64:" "linux-x64:portable" "win-x64:portable")
ver=$(cat VERSION)
rev="$(( $(git rev-list --count HEAD)))-$(git rev-list HEAD | head -n 1 -c 8)"
fullver="${ver}.${rev}"

echo "UPDATING TO VER: $ver.$rev"
PROJ="anime-processing-room-net-core"
sed -i "s/Version>.*</Version>${fullver}</g" "./${PROJ}/Directory.Build.props"
NET_VER=$(cat ${PROJ}/${PROJ}.csproj | grep TargetFramework | sed "s;.*<TargetFramework>\(.*\)</TargetFramework>;\1;g")

for t in ${targets[@]}
do
	target="$(echo $t | cut -d':' -f1 )"
	mode="$(echo $t | cut -d':' -f2 )"
	if [ ! -z "${mode}" ]; then
		mode="-${mode}"
	fi
	echo ""
	echo "PUBLISHING: ${target} ${mode}"
	rm -r "./${PROJ}/bin/Release"
	rm -r "./${PROJ}/obj/Release/${NET_VER}/${t}"
	if [ "${mode}" == "-portable" ]; then
		rm "./publish/AnimeProcessingRoom-v${fullver}-${target}${mode}"
		dotnet publish -r "${target}" -c Release --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true 
	else
		rm "./publish/AnimeProcessingRoom-v${fullver}-${target}"
		dotnet publish -r "${target}" -c Release --self-contained false -p:PublishSingleFile=true
	fi
	rm "./${PROJ}/bin/Release/${NET_VER}/${target}/publish/${PROJ}.pdb"
	for file in $(find "./${PROJ}/bin/Release/${NET_VER}/${target}/publish" | awk "NR>1")
	do
		mv -v "${file}" "publish/`echo "$file" | awk -F'/' '{print $NF}' | sed "s/${PROJ}/AnimeProcessingRoom-v${fullver}-${target}${mode}/g"`"
	done
done